import ROOT
import numpy as np
import h5py
import math
#import JetPlots

import argparse

parser = argparse.ArgumentParser("MLBJESJER")
parser.add_argument('--infile',  type=str, default="infile.root")
parser.add_argument('--rjets',    type=str, default="AntiKt10CSSKUFOTrimmedPtFrac5SmallR20")
parser.add_argument('--tjets',    type=str, default="AntiKt10TruthTrimmedPtFrac5SmallR20")
#parser.add_argument('--outfile', type=str, default="outfile.h5")
myargs = parser.parse_args()

f  = ROOT.TFile.Open(myargs.infile)
t = f.Get("TreeDirectory/JetTree")

# print tree contents
t.Print()

jet_array = np.zeros( (t.GetEntries()*2, 100, 6 ) )

def pass_fiducial_selection(pt1, eta1, phi1, E1, pt2, eta2, phi2, E2, minPt=200, maxPt=9999, maxEta=2.1, balancePt=0.5, verbose=False):
    if(pt1<minPt): 
        if(verbose): print("fail pt",pt1,minPt)
        return False

    if(abs(eta1)>maxEta): 
        if(verbose): print("fail eta 1",abs(eta1),maxEta)
        return False

    if(abs(eta2)>maxEta): 
        if(verbose): print("fail eta 2",abs(eta2),maxEta)
        return False
    
    if(pt2/pt1 < balancePt): 
        if(verbose): print("fail pt balance",pt2/pt1,balancePt)
        return False

    return True
    

def fill_jet_track_array( trk_pts, trk_etas, trk_phis, jet_pt, jet_eta, jet_phi):
    a = np.ones( (100, 3) )*-99
    for track in range(0,100): # gotta -99 pad
        if(track < len(trk_pts)):
            pt = trk_pts[track]
            eta = trk_etas[track]-jet_eta
            phi = trk_phis[track]-jet_phi
            a[track,0] = pt*1.e3
            a[track,1] = eta
            a[track,2] = phi
        else:
            a[track,0] = 0.  # has to be zero for normalisation to work
            a[track,1] = -99
            a[track,2] = -99
    if(len(trk_pts)>0): a[:,0]/=np.sum(a[:,0],0)

    # Sort the tracks by their pT
    a = a[a[:,0].argsort(kind='mergesort')]
    a[:,0] = np.flip(a[:,0])
    a[:,1] = np.flip(a[:,1])
    a[:,2] = np.flip(a[:,2])
    
    # Flip 'em: we always want the second track to be on the same side of the leading track
<<<<<<< HEAD
=======
    """
>>>>>>> origin/master
    if((a[0,0] > 0) and (a[1,0] > 0)):
        eta_leading_track = a[0,1]
        eta_second_track =  a[1,1]
        if(eta_second_track  < eta_leading_track):
            a[:,1] = a[:,1]*-1
            
        phi_leading_track = a[0,2]
        phi_second_track  = a[1,2]
        if(phi_second_track < phi_leading_track):
            a[:,2] = a[:,2]*-1
<<<<<<< HEAD
=======
    """
>>>>>>> origin/master

    # Make sure the padded entries stay zero
    for i in range(0,3):
        for x in range(0,len(a[:,i])):
            if(i==0): # pt is a special case
                if  (a[x,i+1]<=-95): a[x,i]=-99
                elif(a[x,i+1]>=95): a[x,i]=-99
            else:
                if(a[x,i]<=-95): a[x,i]=-99
                elif(a[x,i]>=95): a[x,i]=-99
    return a

filled_jets=0
for event in range(0, t.GetEntries()):
    t.GetEntry(event)
<<<<<<< HEAD
    if(event%10000==0): 
        print('event ',event," / ",t.GetEntries()," efficiency: ",(filled_jets+1)/(event+1)*100)

    if(len(t.rjet1_Pt)<1): continue
    if(len(t.rjet2_Pt)<1): continue   

    # reco selection
    if(not pass_fiducial_selection(t.rjet1_Pt[0],
                                   t.rjet1_Eta[0],
                                   t.rjet1_Phi[0],
                                   t.rjet1_E[0],
                                   t.rjet2_Pt[0],
                                   t.rjet2_Eta[0],
                                   t.rjet2_Phi[0],
                                   t.rjet2_E[0])
    ): continue

    # truth selection
    if(not pass_fiducial_selection(t.tjet1_Pt,
                                   t.tjet1_Eta,
                                   t.tjet1_Phi,
                                   t.tjet1_E,
                                   t.tjet2_Pt,
                                   t.tjet2_Eta,
                                   t.tjet2_Phi,
                                   t.tjet2_E)
=======
    if(event%1000==0): 
        print('event ',event," / ",t.GetEntries()," efficiency: ",(filled_jets+1)/(2*event+1)*100)

    if(len(getattr(t, "tvar_jet_"+myargs.rjets+"_pt"))<2): continue
    if(len(getattr(t, "tvar_jet_"+myargs.tjets+"_pt"))<2): continue

    # reco selection
    if(not pass_fiducial_selection(getattr(t,"tvar_jet_"+myargs.rjets+"_pt")[0],
                                   getattr(t,"tvar_jet_"+myargs.rjets+"_eta")[0],
                                   getattr(t,"tvar_jet_"+myargs.rjets+"_phi")[0],
                                   getattr(t,"tvar_jet_"+myargs.rjets+"_m")[0],
                                   getattr(t,"tvar_jet_"+myargs.rjets+"_pt")[1],
                                   getattr(t,"tvar_jet_"+myargs.rjets+"_eta")[1],
                                   getattr(t,"tvar_jet_"+myargs.rjets+"_phi")[1],
                                   getattr(t,"tvar_jet_"+myargs.rjets+"_m")[1])
    ): continue

    # truth selection
    if(not pass_fiducial_selection(getattr(t,"tvar_jet_"+myargs.tjets+"_pt")[0],
                                   getattr(t,"tvar_jet_"+myargs.tjets+"_eta")[0],
                                   getattr(t,"tvar_jet_"+myargs.tjets+"_phi")[0],
                                   getattr(t,"tvar_jet_"+myargs.tjets+"_m")[0],
                                   getattr(t,"tvar_jet_"+myargs.tjets+"_pt")[1],
                                   getattr(t,"tvar_jet_"+myargs.tjets+"_eta")[1],
                                   getattr(t,"tvar_jet_"+myargs.tjets+"_phi")[1],
                                   getattr(t,"tvar_jet_"+myargs.tjets+"_m")[1]                               )
>>>>>>> origin/master
    ): continue

    flip_reco_truth_matching_order = False # TODO implement matching here
    tlv_reco_1 = ROOT.TLorentzVector()
    tlv_reco_2 = ROOT.TLorentzVector()
    tlv_truth_1 = ROOT.TLorentzVector()
    tlv_truth_2 = ROOT.TLorentzVector()
    
<<<<<<< HEAD
    tlv_reco_1.SetPtEtaPhiE(t.rjet1_Pt[0], t.rjet1_Eta[0], t.rjet1_Phi[0], t.rjet1_E[0])
    tlv_reco_2.SetPtEtaPhiE(t.rjet2_Pt[0], t.rjet2_Eta[0], t.rjet2_Phi[0], t.rjet2_E[0])
    tlv_truth_1.SetPtEtaPhiE(t.tjet1_Pt, t.tjet1_Eta, t.tjet1_Phi, t.tjet1_E)
    tlv_truth_2.SetPtEtaPhiE(t.tjet2_Pt, t.tjet2_Eta, t.tjet2_Phi, t.tjet2_E)
=======
    tlv_reco_1.SetPtEtaPhiM(getattr(t,"tvar_jet_"+myargs.rjets+"_pt")[0],
                            getattr(t,"tvar_jet_"+myargs.rjets+"_eta")[0],
                            getattr(t,"tvar_jet_"+myargs.rjets+"_phi")[0],
                            getattr(t,"tvar_jet_"+myargs.rjets+"_m")[0])

    tlv_reco_2.SetPtEtaPhiM(getattr(t,"tvar_jet_"+myargs.rjets+"_pt")[1],
                            getattr(t,"tvar_jet_"+myargs.rjets+"_eta")[1],
                            getattr(t,"tvar_jet_"+myargs.rjets+"_phi")[1],
                            getattr(t,"tvar_jet_"+myargs.rjets+"_m")[1])

    tlv_truth_1.SetPtEtaPhiM(getattr(t,"tvar_jet_"+myargs.tjets+"_pt")[0],
                             getattr(t,"tvar_jet_"+myargs.tjets+"_eta")[0],
                             getattr(t,"tvar_jet_"+myargs.tjets+"_phi")[0],
                             getattr(t,"tvar_jet_"+myargs.tjets+"_m")[0])
    
    tlv_truth_2.SetPtEtaPhiM(getattr(t,"tvar_jet_"+myargs.tjets+"_pt")[1],
                             getattr(t,"tvar_jet_"+myargs.tjets+"_eta")[1],
                             getattr(t,"tvar_jet_"+myargs.tjets+"_phi")[1],
                             getattr(t,"tvar_jet_"+myargs.tjets+"_m")[1])
>>>>>>> origin/master
    
    dr_11 = tlv_reco_1.DeltaR(tlv_truth_1)
    dr_12 = tlv_reco_1.DeltaR(tlv_truth_2)
    
    if(dr_11 > dr_12): flip_reco_truth_matching_order = True
    
<<<<<<< HEAD
    reco_jet_track_array_1 = fill_jet_track_array(t.rjet1_trk_pt[0],
                                                  t.rjet1_trk_eta[0],
                                                  t.rjet1_trk_phi[0],
                                                  t.rjet1_Pt[0],
                                                  t.rjet1_Eta[0],
                                                  t.rjet1_Phi[0])
        
    truth_jet_track_array_1 = fill_jet_track_array(t.tjet1_trk_pt,
                                                   t.tjet1_trk_eta,
                                                   t.tjet1_trk_phi,
                                                   t.tjet1_Pt,
                                                   t.tjet1_Eta,
                                                   t.tjet1_Phi)            
    
    reco_jet_track_array_2 = fill_jet_track_array(t.rjet2_trk_pt[0],
                                                  t.rjet2_trk_eta[0],
                                                  t.rjet2_trk_phi[0],
                                                  t.rjet2_Pt[0],
                                                  t.rjet2_Eta[0],
                                                  t.rjet2_Phi[0])
    
    truth_jet_track_array_2 = fill_jet_track_array(t.tjet2_trk_pt,
                                                   t.tjet2_trk_eta,
                                                   t.tjet2_trk_phi,
                                                   t.tjet2_Pt,
                                                   t.tjet2_Eta,
                                                   t.tjet2_Phi)
=======
    reco_jet_track_array_1 = fill_jet_track_array(getattr(t,"tvar_jet_"+myargs.rjets+"_constit_pt")[0],
                                                  getattr(t,"tvar_jet_"+myargs.rjets+"_constit_eta")[0],
                                                  getattr(t,"tvar_jet_"+myargs.rjets+"_constit_phi")[0],
                                                  getattr(t,"tvar_jet_"+myargs.rjets+"_pt")[0],
                                                  getattr(t,"tvar_jet_"+myargs.rjets+"_eta")[0],
                                                  getattr(t,"tvar_jet_"+myargs.rjets+"_phi")[0])
        
    truth_jet_track_array_1 = fill_jet_track_array(getattr(t,"tvar_jet_"+myargs.tjets+"_constit_pt")[0],
                                                   getattr(t,"tvar_jet_"+myargs.tjets+"_constit_eta")[0],
                                                   getattr(t,"tvar_jet_"+myargs.tjets+"_constit_phi")[0],
                                                   getattr(t,"tvar_jet_"+myargs.tjets+"_pt")[0],
                                                   getattr(t,"tvar_jet_"+myargs.tjets+"_eta")[0],
                                                   getattr(t,"tvar_jet_"+myargs.tjets+"_phi")[0])
    
    reco_jet_track_array_2 = fill_jet_track_array(getattr(t,"tvar_jet_"+myargs.rjets+"_constit_pt")[1],
                                                  getattr(t,"tvar_jet_"+myargs.rjets+"_constit_eta")[1],
                                                  getattr(t,"tvar_jet_"+myargs.rjets+"_constit_phi")[1],
                                                  getattr(t,"tvar_jet_"+myargs.rjets+"_pt")[1],
                                                  getattr(t,"tvar_jet_"+myargs.rjets+"_eta")[1],
                                                  getattr(t,"tvar_jet_"+myargs.rjets+"_phi")[1]   )
    
    truth_jet_track_array_2 = fill_jet_track_array(getattr(t,"tvar_jet_"+myargs.tjets+"_constit_pt")[1],
                                                   getattr(t,"tvar_jet_"+myargs.tjets+"_constit_eta")[1],
                                                   getattr(t,"tvar_jet_"+myargs.tjets+"_constit_phi")[1],
                                                   getattr(t,"tvar_jet_"+myargs.tjets+"_pt")[1],
                                                   getattr(t,"tvar_jet_"+myargs.tjets+"_eta")[1],
                                                   getattr(t,"tvar_jet_"+myargs.tjets+"_phi")[1]   )
>>>>>>> origin/master

    if(not flip_reco_truth_matching_order):
        temp_array = truth_jet_track_array_1
        truth_jet_track_array_1 = truth_jet_track_array_2
        truth_jet_track_array_2 = temp_array

<<<<<<< HEAD
    """
    if(reco_jet_track_array_1[:,0][0]==-99.): 
        print(reco_jet_track_array_1)
    if(reco_jet_track_array_2[:,0][0]==-99.):
        print(reco_jet_track_array_2)
    if(truth_jet_track_array_1[:,0][0]==-99.):
        print(truth_jet_track_array_1)
    if(truth_jet_track_array_2[:,0][0]==-99.):
        print(truth_jet_track_array_2)
    """

=======
>>>>>>> origin/master
    if not((reco_jet_track_array_1[:,0][0]==-99.) and (truth_jet_track_array_1[:,0][0]==-99.)):
        jet_array[filled_jets,:,0] = reco_jet_track_array_1[:,0]
        jet_array[filled_jets,:,1] = reco_jet_track_array_1[:,1]
        jet_array[filled_jets,:,2] = reco_jet_track_array_1[:,2]
        jet_array[filled_jets,:,3] = truth_jet_track_array_1[:,0]
        jet_array[filled_jets,:,4] = truth_jet_track_array_1[:,1]
        jet_array[filled_jets,:,5] = truth_jet_track_array_1[:,2]

        filled_jets+=1

    if not((reco_jet_track_array_2[:,0][0]==-99.) and (truth_jet_track_array_2[:,0][0]==-99.)):
        jet_array[filled_jets,:,0] = reco_jet_track_array_2[:,0]
        jet_array[filled_jets,:,1] = reco_jet_track_array_2[:,1]
        jet_array[filled_jets,:,2] = reco_jet_track_array_2[:,2]
        jet_array[filled_jets,:,3] = truth_jet_track_array_2[:,0]
        jet_array[filled_jets,:,4] = truth_jet_track_array_2[:,1]
        jet_array[filled_jets,:,5] = truth_jet_track_array_2[:,2]

        filled_jets+=1

print(jet_array)
print(jet_array.shape)

#jet_array=jet_array[jet_array!=0]

print(filled_jets," entries to remove")

jet_array=jet_array[0:-filled_jets]

print(jet_array)
print(jet_array.shape)

<<<<<<< HEAD
h5f_cluster = h5py.File(myargs.outfile, 'w')
=======
h5f_cluster = h5py.File(myargs.rjets+".h5", 'w')
>>>>>>> origin/master
h5f_cluster.create_dataset('jet_array', data=jet_array)
h5f_cluster.close()
