import math
import os
import re
import numpy as np
import h5py
import argparse
import glob

# energyflow imports
import energyflow as ef
from energyflow.archs import EFN
from energyflow.datasets import qg_jets
from energyflow.utils import data_split, remap_pids, to_categorical

from keras.models import load_model

# attempt to import sklearn
try:
    from sklearn.metrics import roc_auc_score, roc_curve
except:
    print('please install scikit-learn in order to make ROC curves')
    roc_curve = False

# attempt to import matplotlib
try:
    import matplotlib.pyplot as plt
except:
    print('please install matploltib in order to make plots')
    plt = False

parser = argparse.ArgumentParser("MLBJESJER")
parser.add_argument('--infilesA', type=str, default="infile.A.root")
parser.add_argument('--infilesB', type=str, default="infile.B.root")
parser.add_argument('--titleA', type=str, default="sample A")
parser.add_argument('--titleB', type=str, default="sample B")
parser.add_argument('--epochs', type=int, default=50)
parser.add_argument('--batch_size', type=int, default=500)
parser.add_argument('--latent_dim', type=int, default=7)
myargs = parser.parse_args()

in_file_A = myargs.infilesA
in_file_B = myargs.infilesB

jets_A = []
files_A = glob.glob(myargs.infilesA)
print(files_A)
for fA in files_A:
    h5f_A = h5py.File(fA,'r')
    if("Sherpa" in myargs.titleA): jets_A.extend(h5f_A['jet_array'][0:195704]) # crude downsampling
    elif("Herwig" in myargs.titleA): jets_A.extend(h5f_A['jet_array'][0:(702568+101903)])
    else: jets_A.extend(h5f_A['jet_array'])
    h5f_A.close()

print("Jets A length:",len(jets_A))
    
jets_B = []
files_B = glob.glob(myargs.infilesB)
print(files_B)
for fB in files_B:
    h5f_B = h5py.File(fB,'r')
    if("Sherpa" in myargs.titleB): jets_B.extend(h5f_B['jet_array'][0:(39140+4900)]) # crude downsampling
    elif("Herwig" in myargs.titleB): jets_B.extend(h5f_B['jet_array'])
    else: jets_B.extend(h5f_B['jet_array'])
    h5f_B.close()

print("Jets B length:",len(jets_B))
    
jet_array_A = np.asarray(jets_A)
jet_array_B = np.asarray(jets_B)

print(jet_array_A.shape)
print(jet_array_B.shape)

# HACK HACK HACK MLB HACK
jet_array_A = np.squeeze(jet_array_A)
jet_array_B = np.squeeze(jet_array_B)

# with truth?
tjet_array_A = jet_array_A[:,:,3:6]
tjet_array_B = jet_array_B[:,:,3:6]

# with reco?
jet_array_A = jet_array_A[:,:,0:3]
jet_array_B = jet_array_B[:,:,0:3]

print("reco ",jet_array_A.shape)
print("reco ",jet_array_B.shape)

print("truth ",tjet_array_A.shape)
print("truth ",jet_array_B.shape)

# prepare inputs
X = np.concatenate((jet_array_A, jet_array_B), axis=0)
print(X.shape)
y = np.concatenate((np.zeros(jet_array_A.shape[0]), 
                    np.ones(jet_array_B.shape[0])),
                   axis=0)
print(y.shape)
print(y)

#Convert labels to categorical. This is a "one-hot" encoding with 0 index being background and 1 index being signal
Y = to_categorical(y, num_classes=2)
print('Shape of labels: {}\n'.format(Y.shape))
print(Y[0:3])  # Background
print(Y[-5:-1])  # Signal  assert X.shape[0] == Y.shape[0]
print(X.shape[-1])
print(Y.shape[-1])
#assert X.shape[-1] == Y.shape[-1]

# Open the stored model
gen='gen'
if('Sherpa' in myargs.titleA): gen='sherpa',
if('Herwig' in myargs.titleA): gen='herwig'
model_file_name = 'EFN_Truth_latent{latent_exp}_ppm{num_ppm_layers}_dense{num_dense_layers}_batch{batch_size}_epochs{num_epochs}_'.format(
    latent_exp=int(myargs.latent_dim),
    num_ppm_layers=3,
    num_dense_layers=3,
    batch_size=int(myargs.batch_size),
    num_epochs=int(myargs.epochs),
)
gen=''
if('Sherpa' in myargs.titleA): gen='sherpa'
elif('Herwig' in myargs.titleA): gen='herwig'
model_file_name+=gen
model_file_name+='.h5'

model = load_model('models/' + model_file_name)
model.summary()

# do train/val/test split
number_of_features=2
#train_frac, val_frac, test_frac = 0.01,0.01,0.98
#(z_train, z_val, z_test,
#  p_train, p_val, p_test,
#  Y_train, Y_val, Y_test) = data_split(X[:,:,0], X[:,:,1:], Y, val=val_frac, test=test_frac)

z_test=X[:,:,0]
p_test=X[:,:,1:]
Y_test=Y

loss, acc = model.evaluate([z_test, p_test], Y_test)
print("Model accuracy: {:5.2f}%".format(100*acc))

# get predictions on test data
print("reco z length ",z_test.shape)
preds = model.predict([z_test, p_test], batch_size=1000)
pfn_false_positive, pfn_true_positive, threshs = roc_curve(
    Y_test[:, 1], preds[:, 1])
auc = roc_auc_score(Y_test[:, 1], preds[:, 1])
print('\nPFN AUC: {AUC}\n'.format(AUC=auc))
print("reco preds length ",preds.shape)

######################### ROC Curve Plot #########################

# plt.rcParams['font.family'] = 'serif'
# plt.rcParams['figure.autolayout'] = True

fig, axes = plt.subplots(1, 2, figsize=(10, 5))

# plot the ROC curves
axes[0].plot(pfn_true_positive, 1-pfn_false_positive,
             '-', color='black', label='PFN')
axes[1].semilogy(pfn_true_positive, 1./pfn_false_positive,
                 '-', color='black', label='PFN')

# axes labels
axes[0].set_xlabel('B Efficiency $(\epsilon_{\mathrm{sig}})$')
axes[0].set_ylabel('A Rejection $(1-\epsilon_{\mathrm{bkg}})$')

axes[1].set_xlabel('B Efficiency $(\epsilon_{\mathrm{sig}})$')
axes[1].set_ylabel('A Rejection $(1/\epsilon_{\mathrm{bkg}})$')

# axes limits
axes[0].set_xlim(0, 1)
axes[0].set_ylim(0, 1)

axes[1].set_xlim(0, 1)

# make legend and show plot
axes[0].legend(loc='best', frameon=False)
axes[1].legend(loc='best', frameon=False)

os.system('mkdir figures/'+model_file_name[:-3]+'/')
plt.savefig('figures/'+model_file_name[:-3]+'/ROC.pdf')
plt.close()

info_file = open('figures/'+model_file_name[:-3]+'/info.txt',"a") 
info_file.write(str(auc))

###################### Plot of network response ##################

print(preds[:, 1][Y_test[:,1]==1])
print(preds[:, 1][Y_test[:,1]==0])

sig_preds=preds[:, 1][Y_test[:,1]==1]
bkg_preds=preds[:, 1][Y_test[:,1]==0]

for e in range(0,len(sig_preds)):
    if(sig_preds[e]>=0.5 and sig_preds[e]<=0.51): sig_preds[e]=-1.0
    if(bkg_preds[e]>=0.5 and bkg_preds[e]<=0.51): bkg_preds[e]=-1.0

if('Sherpa' in myargs.titleA):
    plt.hist(sig_preds, bins=100, range=(0.3,0.8), density=True, histtype='step', color='red', label="Sherpa (Lund)")#, log=True)
    plt.hist(bkg_preds, bins=100, range=(0.3,0.8), density=True, histtype='step', color='coral', label="Sherpa (AHADIC)")#,  log=True)
if('Herwig' in myargs.titleA):
    plt.hist(sig_preds, bins=100, range=(0.3,0.8), density=True, histtype='step', color='darkgreen', label="Herwig (Dipole)")#,  log=True)
    plt.hist(bkg_preds, bins=100, range=(0.3,0.8), density=True, histtype='step', color='limegreen', label="Herwig (Ang. org.)")#,  log=True)
plt.xlabel('EFN Response')
plt.ylabel('Arb. units')
plt.legend(loc='upper right')
plt.savefig('figures/'+model_file_name[:-3]+'/response.png')
plt.close()

###################### Plot of network response, but with truth jets ##################

print("\nTRUTH STUFF\n")

# prepare inputs
X = np.concatenate((tjet_array_A, tjet_array_B), axis=0)
print(X.shape)
y = np.concatenate((np.zeros(tjet_array_A.shape[0]),
                    np.ones(tjet_array_B.shape[0])),
                   axis=0)
print(y.shape)
print(y)

#Convert labels to categorical. This is a "one-hot" encoding with 0 index being background and 1 index being signal
Y = to_categorical(y, num_classes=2)
print('Shape of labels: {}\n'.format(Y.shape))
print(Y[0:3])  # Background
print(Y[-5:-1])  # Signal  assert X.shape[0] == Y.shape[0]
print(X.shape[-1])
print(Y.shape[-1])
#assert X.shape[-1] == Y.shape[-1]

# do train/val/test split
#number_of_features=2
#train_frac, val_frac, test_frac = 0.01,0.01,0.98
#(z_train, z_val, z_test,
# p_train, p_val, p_test,
# Y_train, Y_val, Y_test) = data_split(X[:,:,0], X[:,:,1:], Y, val=val_frac, test=test_frac)

z_test=X[:,:,0]
p_test=X[:,:,1:]
Y_test=Y

print("truth z length ",z_test.shape)

loss, acc = model.evaluate([z_test, p_test], Y_test)
print("Model accuracy: {:5.2f}%".format(100*acc))

# get predictions on test data
preds = model.predict([z_test, p_test], batch_size=1000)
pfn_false_positive, pfn_true_positive, threshs = roc_curve(
        Y_test[:, 1], preds[:, 1])
auc = roc_auc_score(Y_test[:, 1], preds[:, 1])
print('\nPFN AUC: {AUC}\n'.format(AUC=auc))
print("truth preds length",preds.shape)

print(preds[:, 1][Y_test[:,1]==1])
print(preds[:, 1][Y_test[:,1]==0])

tsig_preds=preds[:, 1][Y_test[:,1]==1]
tbkg_preds=preds[:, 1][Y_test[:,1]==0]

print("sig preds shape",sig_preds.shape)
print("bkg preds shape",bkg_preds.shape)
print("tsig preds shape",tsig_preds.shape)
print("tbkg preds shape",tbkg_preds.shape)

for e in range(0,len(tsig_preds)):
    if(tsig_preds[e]>=0.5 and tsig_preds[e]<=0.51): tsig_preds[e]=-1.0
    if(tbkg_preds[e]>=0.5 and tbkg_preds[e]<=0.51): tbkg_preds[e]=-1.0

from matplotlib.colors import LogNorm
if('Sherpa' in myargs.titleA):
    plt.hist(tsig_preds, bins=100, range=(0.3,0.8), density=True, histtype='step', color='red', label="Sherpa (Lund)", linestyle='-')#,  log=True)
    plt.hist(tbkg_preds, bins=100, range=(0.3,0.8), density=True, histtype='step', color='coral', label="Sherpa (AHADIC)", linestyle='-')#,  log=True)
if('Herwig' in myargs.titleA):
    plt.hist(tsig_preds, bins=100, range=(0.3,0.8), density=True, histtype='step', color='darkgreen', label="Herwig (Dipole)", linestyle='-')#,  log=True)
    plt.hist(tbkg_preds, bins=100, range=(0.3,0.8), density=True, histtype='step', color='limegreen', label="Herwig (Ang. org.)", linestyle='-')#,  log=True)
plt.xlabel('EFN Response')
plt.ylabel('Arb. units')
plt.legend(loc='upper right')
plt.savefig('figures/'+model_file_name[:-3]+'/response_truth.png')
plt.close()

# reco and truth level network response

plt.hist2d(sig_preds, tsig_preds, bins=100, density=True, cmap='viridis', label="Herwig (Dipole)", norm=LogNorm())
plt.savefig('figures/'+model_file_name[:-3]+'/response_matrix_sig.png')
plt.close()

plt.hist2d(bkg_preds, tbkg_preds, bins=100, density=True, cmap='viridis', label="Herwig (Angular)", norm=LogNorm())
plt.savefig('figures/'+model_file_name[:-3]+'/response_matrix_bkg.png')
plt.close()

# correlations between network response and input features

ntrkA = np.zeros(len(p_test[:,0]))
ntrkB = np.zeros(len(p_test[:,0]))

if('Sherpa' in myargs.titleA):
    plt.hist2d(p_test[:,0][Y_test[:,1]==1], sig_preds, bins=100, range=(0.0,1.0), density=True, histtype='step', color='red', label="Sherpa (Lund)")
    plt.hist2d(p_test[:,0][Y_test[:,1]==0], bkg_preds, bins=100, range=(0.0,1.0), density=True, histtype='step', color='coral', label="Sherpa (AHADIC)")
    plt.xlabel('NTrk')
    plt.ylabel('EFN Response')
    plt.legend(loc='upper right')
    plt.savefig('figures/'+model_file_name[:-3]+'/response.png')
    plt.close()

print(len(ntrkA),len(sig_preds))

if('Herwig' in myargs.titleA):
    for e in range(0,len(p_test)):
        for t in range(0,len(p_test[e])):
        #print(z_test[e,t,0]*(Y_test[t,1]==1),t)
            if((z_test[e,t]*(Y_test[t,1]==1)>0)): ntrkA[e]+=1
            if((z_test[e,t]*(Y_test[t,1]==0)>0)): ntrkB[e]+=1
            #print(ntrkA[e])
    #ntrkA=np.delete(ntrkA, np.where(ntrkA == [0.]))
    #ntrkB=np.delete(ntrkB, np.where(ntrkB == [0.]))
    print(len(ntrkA),len(sig_preds))
    print(len(ntrkB),len(bkg_preds))
    print(ntrkA,ntrkB)

    plt.hist(ntrkA, bins=101, range=(0.0,100.0), density=True, histtype='step', color='darkgreen', label="Herwig (Angular)", linestyle='-')
    plt.hist(ntrkB, bins=101, range=(0.0,100.0), density=True, histtype='step', color='limegreen', label="Herwig (Dipole)", linestyle='-')
    plt.xlabel('NTrk')
    plt.ylabel('arb. units')
    plt.savefig('figures/'+model_file_name[:-3]+'/ntrk.png')
    plt.close()
    
    plt.hist2d(ntrkA, sig_preds, cmap='viridis', label="Herwig (Dipole)", range=[[0.0,1.0],[0,1.0]], density=True)
    plt.xlabel('NTrk')
    plt.ylabel('EFN Response')
    plt.legend(loc='upper right')
    plt.savefig('figures/'+model_file_name[:-3]+'/response_vs_ntrk_dipole.png')
    plt.close()
    plt.hist2d(ntrkB, bkg_preds, range=[[0.0,1.0],[0,1.0]], density=True, cmap='viridis', label="Herwig (Ang. org.)",data=None)
    plt.xlabel('NTrk')
    plt.ylabel('EFN Response')
    plt.legend(loc='upper right')
    plt.savefig('figures/'+model_file_name[:-3]+'/response_vs_ntrk_angular.png')
    plt.close()

##################################################################
