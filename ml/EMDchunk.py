import numpy as np
import h5py
import argparse

import energyflow as ef

def getJets(file, startJet, chunkSize, reco = 1):
    h5f = h5py.File(file,'r')
    jets = []
    jets.extend(h5f['jet_array'][startJet:startJet+chunkSize])

    jet_array = np.asarray(jets)
    jet_array = np.squeeze(jet_array)
    jet_array = jet_array[:,:,0:3] if reco else jet_array[:,:,3:6]
    jetList = jet_array.tolist()
    jetList = [[j for j in i if j != [-99.0, -99.0, -99.0]] for i in jetList]
    return jetList

parser = argparse.ArgumentParser("EMDchunk")
parser.add_argument('--infile1', type=str)
parser.add_argument('--infile2', type=str)
parser.add_argument('--label1', type=str)
parser.add_argument('--label2', type=str)
parser.add_argument('--startJet1', type=int, default=0)
parser.add_argument('--startJet2', type=int, default=0)
parser.add_argument('--chunkSize', type=int, default=20000)
parser.add_argument('--recoFile1', type=int, default=1)
parser.add_argument('--recoFile2', type=int, default=1)
myargs = parser.parse_args()

chunkSize = myargs.chunkSize

infile1 = myargs.infile1
infile2 = myargs.infile2

recoFile1 = myargs.recoFile1
recoFile2 = myargs.recoFile2

if(infile1.find('AntiKt10Truth') >= 0):
    infile1 = infile1.replace('AntiKt10Truth', 'AntiKt10CSSKUFO')
    recoFile1 = 0
if(infile2.find('AntiKt10Truth') >= 0):
    infile2 = infile2.replace('AntiKt10Truth', 'AntiKt10CSSKUFO')
    recoFile2 = 0

jets1 = getJets(infile1, myargs.startJet1, chunkSize, recoFile1)
label1 = myargs.label1.split('/')[-1]

jets2 = getJets(infile2, myargs.startJet2, chunkSize, recoFile2)
label2 = myargs.label2.split('/')[-1]

print('calculating EMD for '+infile1+' and '+infile2+' for '+str(chunkSize)+' jets.')

emds = ef.emd.emds(jets1, jets2, R=1.0, verbose=1, print_every=10000)

emds = np.array(emds, dtype=np.float16)

h5f_out = h5py.File(label1+'_'+str(myargs.startJet1)+'_'+label2+'_'+str(myargs.startJet2)+'.h5', 'w')
h5f_out.create_dataset('emds', data=emds)
h5f_out.close()
