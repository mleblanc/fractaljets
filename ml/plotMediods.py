import h5py
import argparse
import numpy as np
import matplotlib.pyplot as plt

def getJets(file, startJet, chunkSize, reco = 1):
    h5f = h5py.File(file,'r')
    jets = []
    jets.extend(h5f['jet_array'][startJet:startJet+chunkSize])

    jet_array = np.asarray(jets)
    jet_array = np.squeeze(jet_array)
    jet_array = jet_array[:,:,0:3] if reco else jet_array[:,:,3:6]
    jetList = jet_array.tolist()
    jetList = [[j for j in i if j != [-99.0, -99.0, -99.0]] for i in jetList]
    return jetList

def rotate_point(point, angle, center_point=(0, 0), convert_to_rads=False):
    """Rotates a point around center_point(origin by default)
    Angle is in degrees.
    Rotation is counter-clockwise
    """

    if(convert_to_rads): angle_rad = np.radians(angle % 360)
    else: angle_rad = angle
    # Shift the point so that center_point becomes the origin
    new_point = (point[0] - center_point[0], point[1] - center_point[1])
    new_point = (new_point[0] * np.cos(angle_rad) - new_point[1] * np.sin(angle_rad),
                 new_point[0] * np.sin(angle_rad) + new_point[1] * np.cos(angle_rad))
    # Reverse the shifting we have done
    new_point = (new_point[0] + center_point[0], new_point[1] + center_point[1])
    return new_point

def plotJet(jetList, color, recenter=True, rotate=True, flip=True):
    pts = [i[0] for i in jetList]
    ys = [i[1] for i in jetList]
    phis = [i[2] for i in jetList]

    theta = np.pi-np.arctan(ys[1]/phis[1])

    if(recenter):
        for y in range(0,len(ys)):
            if(ys[y]>-95):
                ys[y]-=ys[0]
        for phi in range(0,len(phis)):
            if(phis[phi]>-95):
                phis[phi]-=phis[0]


    rotated_ys=[]
    rotated_phis=[]
    if(rotate):
        for point in range(0,len(pts)):
            new_coord = rotate_point((ys[point],phis[point]), -theta)
            rotated_ys.append(new_coord[0])
            rotated_phis.append(new_coord[1])
    else:
        rotated_ys=ys
        rotated_phis=phis

    if(flip):
        # determine whether to flip it or nah
        do_flip_phi = False
        pt_sum_phi = 0.
        for phi in range(0,len(rotated_phis)):
            if(rotated_phis[phi]>-95): pt_sum_phi+=pts[phi]*np.sign(rotated_phis[phi])
                    
        if(pt_sum_phi>0): do_flip_phi = True
        if(do_flip_phi):
            for phi in range(0,len(rotated_phis)):
                if(rotated_phis[phi]>-95): rotated_phis[phi]*=-1
                
                    
        do_flip_y = False
        pt_sum_y = 0.
        for y in range(0,len(rotated_ys)):
            if(rotated_phis[y]>-95): pt_sum_y+=pts[y]*np.sign(rotated_ys[y])

        if(pt_sum_y>0): do_flip_y = True
        if(do_flip_y):
            for y in range(0,len(rotated_ys)):
                if(rotated_ys[y]>-95): rotated_ys[y]*=-1
                        
        print(pt_sum_phi,pt_sum_y)


    plt.scatter(rotated_ys, rotated_phis, marker='o', s=[1000*pts[i] for i in range(len(pts))], color=color)
    R=1.1
    plt.xlim(-R, R); plt.ylim(-R, R)
    circle1 = plt.Circle((0, 0), 1.0, color=color, fill=0)
    ax = plt.gca()
    plt.gca().set_aspect('equal', adjustable='box')
    plt.axis('off')
    ax.add_artist(circle1)

parser = argparse.ArgumentParser("plotMediods")
parser.add_argument('--infileEMD', type=str)
parser.add_argument('--nMediods', type=int, default=3)
parser.add_argument('--infileJets', type=str)
parser.add_argument('--startJet', type=int, default=0)
parser.add_argument('--chunkSize', type=int, default=10000)
parser.add_argument('--label', type=str)
myargs = parser.parse_args()

h5f = h5py.File(myargs.infileEMD,'r')
emds = h5f['emds']

avgEMDs = [sum(emds[i]) / len(emds[i]) for i in range(len(emds))]

mediods = np.argsort(avgEMDs)

jets = getJets(myargs.infileJets, myargs.startJet, myargs.chunkSize)

#colors = ['red', 'blue', 'green']
colormap = plt.cm.cool
colors = ([colormap(i) for i in np.linspace(0, 0.9, len(mediods))])

print("There are "+str(len(mediods))+" medoids.")

for i in range(0, myargs.nMediods, 2):
    plt.close()
    plotJet(jets[mediods[i]], colors[i%len(colors)])
    plt.savefig(myargs.label+'_mediod_'+str(i).zfill(3)+'.png')
    print('saved ',myargs.label+'_mediod_'+str(i).zfill(3)+'.png')
