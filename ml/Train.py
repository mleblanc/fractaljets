import math
import os
import re
import numpy as np
import h5py
import argparse
import glob

# energyflow imports
import energyflow as ef
from energyflow.archs import PFN
from energyflow.datasets import qg_jets
from energyflow.utils import data_split, remap_pids, to_categorical

# attempt to import sklearn
try:
    from sklearn.metrics import roc_auc_score, roc_curve
except:
    print('please install scikit-learn in order to make ROC curves')
    roc_curve = False

# attempt to import matplotlib
try:
    import matplotlib.pyplot as plt
except:
    print('please install matploltib in order to make plots')
    plt = False

parser = argparse.ArgumentParser("MLBJESJER")
parser.add_argument('--infilesA', type=str, default="infile.A.root")
parser.add_argument('--infilesB', type=str, default="infile.B.root")
parser.add_argument('--titleA', type=str, default="sample A")
parser.add_argument('--titleB', type=str, default="sample B")
parser.add_argument('--epochs', type=int, default=50)
parser.add_argument('--batch_size', type=int, default=500)
parser.add_argument('--latent_dim', type=int, default=7)
myargs = parser.parse_args()

in_file_A = myargs.infilesA
in_file_B = myargs.infilesB

jets_A = []
files_A = glob.glob(myargs.infilesA)
print(files_A)
for fA in files_A:
    h5f_A = h5py.File(fA,'r')
    if("Sherpa" in myargs.titleA): jets_A.extend(h5f_A['jet_array'][0:195704]) # crude downsampling
    elif("Herwig" in myargs.titleA): jets_A.extend(h5f_A['jet_array'][0:(702568+101903)])
    else: jets_A.extend(h5f_A['jet_array'])
    h5f_A.close()

print("Jets A length:",len(jets_A))
    
jets_B = []
files_B = glob.glob(myargs.infilesB)
print(files_B)
for fB in files_B:
    h5f_B = h5py.File(fB,'r')
    if("Sherpa" in myargs.titleB): jets_B.extend(h5f_B['jet_array'][0:(39140+4900)]) # crude downsampling
    elif("Herwig" in myargs.titleB): jets_B.extend(h5f_B['jet_array'])
    else: jets_B.extend(h5f_B['jet_array'])
    h5f_B.close()

print("Jets B length:",len(jets_B))
    
jet_array_A = np.asarray(jets_A)
jet_array_B = np.asarray(jets_B)

print(jet_array_A.shape)
print(jet_array_B.shape)

# HACK HACK HACK MLB HACK
jet_array_A = np.squeeze(jet_array_A)
jet_array_B = np.squeeze(jet_array_B)

print(jet_array_A.shape)
print(jet_array_B.shape)

# Mess around with the PFN

################################### SETTINGS ###################################
# the commented values correspond to those in 1810.05165
###############################################################################

# data controls, can go up to 2000000 for full dataset
train, val, test = 25000, 10000, 10000
# train, val, test = 1000000, 200000, 200000
use_pids = True

# network architecture parameters
Phi_sizes, F_sizes = (100, 100, 128), (100, 100, 100)
#Phi_sizes, F_sizes = (100, 100, 256), (100, 100, 100)

# network training parameters
num_epoch = myargs.epochs
batch_size = myargs.batch_size

# prepare inputs
X = np.concatenate((jet_array_A, jet_array_B), axis=0)
print(X.shape)
y = np.concatenate((np.zeros(jet_array_A.shape[0]), 
                    np.ones(jet_array_B.shape[0])),
                   axis=0)
print(y.shape)
print(y)

#Convert labels to categorical. This is a "one-hot" encoding with 0 index being background and 1 index being signal
Y = to_categorical(y, num_classes=2)
print('Shape of labels: {}\n'.format(Y.shape))
print(Y[0:3])  # Background
print(Y[-5:-1])  # Signal  assert X.shape[0] == Y.shape[0]
print(X.shape[-1])
print(Y.shape[-1])
#assert X.shape[-1] == Y.shape[-1]

# data controls, can go up to 2000000 total for full dataset
train_frac, val_frac, test_frac = 0.5, 0.25, 0.25
assert np.sum([train_frac, val_frac, test_frac]) == 1.0

# network architecture parameters
# From https://energyflow.network/docs/archs/#efn
# ppm_sizes: The sizes of the dense layers in the per-particle frontend module phi.
# The last element will be the number of latent observables that the model defines.
latent_space_size_exponent = myargs.latent_dim
ppm_sizes = (100, 100, int(2 ** latent_space_size_exponent))
dense_sizes = (100, 100, 100)

# build architecture
# efn = EFN(input_dim=X.shape[-1], ppm_sizes=ppm_sizes, dense_sizes=dense_sizes)
print("Input dim ",X.shape[-1]," should be 3")
pfn = PFN(input_dim=X.shape[-1], Phi_sizes=ppm_sizes, F_sizes=dense_sizes,
          mask_val=-99,
          latent_dropout=0.25,
          F_dropouts=0) #dense_dropouts seems broken

# do train/val/test split
(X_train, X_val, X_test,
 Y_train, Y_val, Y_test) = data_split(X, Y, val=val_frac, test=test_frac)

#from sklearn.utils import shuffle
#X_train, Y_train = shuffle(X_train, Y_train, random_state=0)

print(X_train[0],Y_train[0])
print(X_train[1],Y_train[1])

# train the model
history = pfn.fit(X_train, Y_train,
                  epochs=num_epoch,
                  batch_size=batch_size,
                  validation_data=(X_val, Y_val),
                  shuffle=True,
                  verbose=1)
model = pfn.model

# get predictions on test data
preds = model.predict(X_test, batch_size=1000)

pfn_false_positive, pfn_true_positive, threshs = roc_curve(
    Y_test[:, 1], preds[:, 1])

auc = roc_auc_score(Y_test[:, 1], preds[:, 1])
print('\nPFN AUC: {AUC}\n'.format(AUC=auc))


# plt.rcParams['font.family'] = 'serif'
# plt.rcParams['figure.autolayout'] = True

######################### Save the Model #########################

 # If a model was loaded then don't resave it
gen='gen'
if('Sherpa' in myargs.titleA): gen='sherpa',
if('Herwig' in myargs.titleA): gen='herwig'
model_file_name = 'PFN_latent{latent_exp}_ppm{num_ppm_layers}_dense{num_dense_layers}_batch{batch_size}_epochs{num_epochs}_'.format(
    latent_exp=int(latent_space_size_exponent),
    num_ppm_layers=len(ppm_sizes),
    num_dense_layers=len(dense_sizes),
    batch_size=int(batch_size),
    num_epochs=int(num_epoch),
)
gen=''
if('Sherpa' in myargs.titleA): gen='sherpa'
elif('Herwig' in myargs.titleA): gen='herwig'
model_file_name+=gen
model_file_name+='.h5'
pfn.model.save('models/'+model_file_name)

######################### ROC Curve Plot #########################

# plt.rcParams['font.family'] = 'serif'
# plt.rcParams['figure.autolayout'] = True

fig, axes = plt.subplots(1, 2, figsize=(10, 5))

# plot the ROC curves
axes[0].plot(pfn_true_positive, 1-pfn_false_positive,
             '-', color='black', label='PFN')
axes[1].semilogy(pfn_true_positive, 1./pfn_false_positive,
                 '-', color='black', label='PFN')

# axes labels
axes[0].set_xlabel('B Efficiency $(\epsilon_{\mathrm{sig}})$')
axes[0].set_ylabel('A Rejection $(1-\epsilon_{\mathrm{bkg}})$')

axes[1].set_xlabel('B Efficiency $(\epsilon_{\mathrm{sig}})$')
axes[1].set_ylabel('A Rejection $(1/\epsilon_{\mathrm{bkg}})$')

# axes limits
axes[0].set_xlim(0, 1)
axes[0].set_ylim(0, 1)

axes[1].set_xlim(0, 1)

# make legend and show plot
axes[0].legend(loc='best', frameon=False)
axes[1].legend(loc='best', frameon=False)

os.system('mkdir figures/'+model_file_name[:-3]+'/')
plt.savefig('figures/'+model_file_name[:-3]+'/ROC.pdf')
plt.close()

info_file = open('figures/'+model_file_name[:-3]+'/info.txt',"a") 
info_file.write(str(auc))
#info_file.write(history.history)

print(history)
print(history.history)

# Plot training & validation loss values
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig('figures/'+model_file_name[:-3]+'/val_loss.pdf')
plt.close()

# Plot training & validation accuracy values
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('Model accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig('figures/'+model_file_name[:-3]+'/val_acc.pdf')
plt.close()

# ---

##################################################################

# Weird filter plots, broken right now

from keras import backend as K
def eval_filters(self, patch, n=100, prune=True):
    """Evaluates the latent space filters of this model on a patch of the
    two-dimensional geometric input space.
    **Arguments**
    - **patch** : {_tuple_, _list_} of _float_
    - Specifies the patch of the geometric input space to be evaluated.
    A list of length 4 is interpretted as `[xmin, ymin, xmax, ymax]`.
    Passing a single float `R` is equivalent to `[-R,-R,R,R]`.
    - **n** : {_tuple_, _list_} of _int_
    - The number of grid points on which to evaluate the filters. A list
    of length 2 is interpretted as `[nx, ny]` where `nx` is the number of
    points along the x (or first) dimension and `ny` is the number of points
    along the y (or second) dimension.
    - **prune** : _bool_
    - Whether to remove filters that are all zero (which happens sometimes
    due to dying ReLUs).
    **Returns**
    - (_numpy.ndarray_, _numpy.ndarray_, _numpy.ndarray_)
    - Returns three arrays, `(X, Y, Z)`, where `X` and `Y` have shape `(nx, ny)`
    and are arrays of the values of the geometric inputs in the specified patch.
    `Z` has shape `(num_filters, nx, ny)` and is the value of the different
    filters at each point.
    """

    # determine patch of xy space to evaluate filters on
    if isinstance(patch, (float, int)):
        if patch > 0:
            xmin, ymin, xmax, ymax = -patch, -patch, patch, patch
        else:
            ValueError('patch must be positive when passing as a single number.')
    else:
        xmin, ymin, xmax, ymax = patch
            
    # determine number of pixels in each dimension
    if isinstance(n, int):
        nx = ny = n
    else:
        nx, ny = n

    # construct grid of inputs
    xs, ys = np.linspace(xmin, xmax, nx), np.linspace(ymin, ymax, ny)
    X, Y = np.meshgrid(xs, ys, indexing='ij')
    XY = np.asarray([X, Y]).reshape((2, nx*ny)).T

    print(XY.shape)

    # construct function
    #print(self._tensors[self._tensor_inds['latent'][0]])
    kf = K.function([self.inputs[0]], [self._tensors[self._tensor_inds['latent'][0] - 1]])
    #kf = K.function([self.inputs[0]], [self.latent[-1]])

    # evaluate function
    s = self.Phi_sizes[-1] if len(self.Phi_sizes) else self.input_dim
    print(kf([[XY]]))
    Z = kf([[XY]])[0][0].reshape(nx, ny, s).transpose((2,0,1))
    
    # prune filters that are off
    if prune:
        return X, Y, Z[[not (z == 0).all() for z in Z]]
    
    return X, Y, Z

filters = eval_filters(pfn, 0.4, n=100)[2]
#filters = ef.archs.EFN.eval_filters(pfn,0.4,n=100)[2]
print(filters)

fig, axs = plt.subplots(12, 12)
for i in range(0,12):
    for j in range(0,12):
        if("Sherpa" in myargs.titleA): axs[i,j].imshow(filters[13*i+j], cmap='inferno', interpolation='nearest')
        if("Herwig" in myargs.titleA): axs[i,j].imshow(filters[13*i+j], cmap='viridis', interpolation='nearest')
        axs[i,j].axis('off')
fig.set_size_inches(18.5, 10.5)
plt.savefig('figures/'+model_file_name[:-3]+'/filters.png')
        
from matplotlib.pyplot import cm
n=128
color=iter(cm.rainbow(np.linspace(0,1,n)))
if("Sherpa" in myargs.titleA): color=iter(cm.inferno(np.linspace(0,1,n)))
if("Herwig" in myargs.titleA): color=iter(cm.viridis(np.linspace(0,1,n)))

for i in range(0,n):
    c=next(color)
    plt.contour(filters[i], 2, colors=[c])
    plt.axis('off')
fig = plt.gcf()
fig.set_size_inches(18.5, 10.5)
fig.savefig('figures/'+model_file_name[:-3]+'/filters_groovy.png', dpi=200)
                
