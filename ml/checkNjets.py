import h5py
import argparse

parser = argparse.ArgumentParser("checkNjets")
parser.add_argument('--infile', type=str)
myargs = parser.parse_args()

h5f = h5py.File(myargs.infile,'r')
jets = []
jets.extend(h5f['jet_array'])
print(len(jets))
