import h5py
import argparse
import numpy as np
import matplotlib.pyplot as plt

def getJets(file, startJet, chunkSize, reco = 1):
    h5f = h5py.File(file,'r')
    jets = []
    jets.extend(h5f['jet_array'][startJet:startJet+chunkSize])

    jet_array = np.asarray(jets)
    jet_array = np.squeeze(jet_array)
    jet_array = jet_array[:,:,0:3] if reco else jet_array[:,:,3:6]
    jetList = jet_array.tolist()
    jetList = [[j for j in i if j != [-99.0, -99.0, -99.0]] for i in jetList]
    return jetList

def plotJet(jetList, color):
    pts = [i[0] for i in jetList]
    ys = [i[1] for i in jetList]
    phis = [i[2] for i in jetList]
    plt.scatter(ys, phis, marker='o', s=[10*pts[i] for i in range(len(pts))], color=color)
    R=1.1
    plt.xlim(-R, R); plt.ylim(-R, R)
    circle1 = plt.Circle((0, 0), 1.0, color=color, fill=0)
    ax = plt.gca()
    plt.gca().set_aspect('equal', adjustable='box')
    plt.axis('off')
    ax.add_artist(circle1)

inputList = ['AntiKt10CSSKUFO']
groomList = ['', 'TrimmedPtFrac5SmallR20', 'PrunedZCut15RCut25', 'SoftDropZCut10Beta0', 'SoftDropZCut10Beta100', 'BottomUpSoftDropZCut5Beta100', 'RecursiveSoftDropZCut5Beta100N-100', 'RecursiveSoftDropZCut5Beta100N100', 'RecursiveSoftDropZCut5Beta100N300']

jetList = []
for input in inputList:
    for groom in groomList:
        jetList.append(input+groom)
print(jetList)

path = '/data0/users/sweber/'

parser = argparse.ArgumentParser("plotMediods")
parser.add_argument('--nMediods', type=int, default=3)
parser.add_argument('--startJet', type=int, default=0)
parser.add_argument('--chunkSize', type=int, default=10000)
parser.add_argument('--label', type=str)
myargs = parser.parse_args()

colors = ['red', 'blue', 'green']

j=0


fig_size = plt.rcParams["figure.figsize"]
fig_size[0] = 12
fig_size[1] = 12
plt.rcParams["figure.figsize"] = fig_size

fig = plt.figure()

for input in jetList:
    print(input)
    h5f = h5py.File(path+'UFOCSSK_EMDs/'+input+'_0_'+input+'_0_1000chunk.h5','r')
    emds = h5f['emds']

    avgEMDs = [sum(emds[i]) / len(emds[i]) for i in range(len(emds))]

    mediods = np.argsort(avgEMDs)

    jets = getJets(path+input+'.h5', myargs.startJet, myargs.chunkSize)

    label = input.replace('AntiKt10CSSKUFO','')
    if(label == ''):
        label = 'Ungroomed'

    for i in range(myargs.nMediods):
        print(i+1+j*myargs.nMediods)
        ax = fig.add_subplot(len(jetList), myargs.nMediods, i+1+j*myargs.nMediods)
        plotJet(jets[mediods[i]], colors[i])

    plt.text(-18, -1.25, label, fontsize=12)
    j += 1

plt.subplots_adjust(wspace=0, hspace=0)
plt.axis('off')
plt.savefig(myargs.label+'.png')
