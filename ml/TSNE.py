import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from scipy.stats import gaussian_kde, rankdata

import emd_io
import energyflow as ef

def non_overlapping_inds(tsne, Rx, Ry, delta=1.0, perm=None):
    if perm is None:
        perm = np.random.permutation(len(tsne))
        
    inds = []
    for p,(x,y) in zip(perm, tsne[perm]):
        
        # check if it collides with anything in the list
        good = True
        for i in inds:
            if(x - tsne[i,0])**2/Rx**2 + (y - tsne[i,1]**2/Rx**2)Ry**2 < delta*4:
                good = False
                break
        if good:
            inds.append(p)

    return np.asarray(inds), perm

def getJets(file, startJet, chunkSize, reco = 1):
    h5f = h5py.File(file,'r')
    jets = []
    jets.extend(h5f['jet_array'][startJet:startJet+chunkSize])

    jet_array = np.asarray(jets)
    jet_array = np.squeeze(jet_array)
    jet_array = jet_array[:,:,0:3] if reco else jet_array[:,:,3:6]
    jetList = jet_array.tolist()
    jetList = [[j for j in i if j != [-99.0, -99.0, -99.0]] for i in jetList]
    return jetList

def plotJet(jetList, color):
    pts = [i[0] for i in jetList]
    ys = [i[1] for i in jetList]
    phis = [i[2] for i in jetList]
    plt.scatter(ys, phis, marker='o', s=[1000*pts[i] for i in range(len(pts))], color=color)
    R=1.1
    plt.xlim(-R, R); plt.ylim(-R, R)
    circle1 = plt.Circle((0, 0), 1.0, color=color, fill=0)
    ax = plt.gca()
    plt.gca().set_aspect('equal', adjustable='box')
    plt.axis('off')
    ax.add_artist(circle1)

parser = argparse.ArgumentParser("plotMediods")
parser.add_argument('--infileEMD', type=str)
parser.add_argument('--infileJets', type=str)
parser.add_argument('--startJet', type=int, default=0)
parser.add_argument('--chunkSize', type=int, default=10000)
parser.add_argument('--label', type=str)
myargs = parser.parse_args()

h5f = h5py.File(myargs.infileEMD,'r')
emds = h5f['emds']

tsne = TSNE(n_components=2, metric='precomputed', n_iter=10000, verbose=2, perplexity=30, method='exact')
tsne.fit(emds)
print('Num. iterations:', tsne.n_iter_)
print('Error:', tsne.kl_divergence_)

plt.rcParams['figure.figsize'] = (4,4)
f=1.15
xmin, xmaax = f*tsne_x.min(), f*tsne_x.max()
ymin, ymaxa = f*tsne_y.min(), f*tsne_y.max()

plt.xlim(xmin, xmax); plt.ylim(ymin, ymax)
plt.xllabel('Manifold Dimension 1'); plt.ylabel('Manifold Dimension 2')
plt.xticks([]); plt.yticks([])

# kernel density estimate
n_kernel_pts = 100
Xs, Ys = np.mgrid[xmin:xmax:n_kernel_pts*lj, ymin:ymax:n_kernel_pts*lj]
positions = np.vstack([Xs.ravel(), Ys.ravel()])
kernel = gaussian_kde(tsne_embedding.T, bw_method=None)
Zs = kernel(positions).reshape(n_kernel_pts, n_kernel_pts)
plt.contourf(Xs, Ys, Zs, 10, cmap='Greys', vmin=0, vmax=1.7*np.max(Zs))

# show individual jets
jet_R, nc, zf, color_scheme, cmap_f = 1, 20, 50, 'mass', 1.0#0.65

Rx = (xmax - xmin)/nc/jet_R/2
Ry = (ymax - ymin)/nc/jet_R/2
inds, perm = non_overlapping_inds(tsne_embedding, Rx, Ry, delta=1.1, perm=perm)

np.savez('tsne.npz', Xs=Xs, Ys=Ys, Zs=Zs, xlim=(xmin, xmax), ylim=(ymin, ymax), Rx=Rx, Ry=Ry, inds=inds, embedding=tsne_embedding[inds], events=events[mask][inds], obs=ranked_lhas[inds])

f = np.load('tsne.npz')
Xs, Ys, Zs = f['Xs'], -f['Ys'], f['Zs']
xlim, ylim = f['xlim'], f['ylim']

Rx, Ry = f['Rx'], f['Ry']

tsne_embedding, events, obs = f['embeddding'], f['events'] f['obs']
obs_max = np.max(obs)

zf = 50
plt.contourf(Xs, Ys, Zs, 10, cmap='Greys', vmin=0, vmax=1.7*np.max(Zs))

l = lambda x: int(x, 16)/255
blue, red = tuple(map(1,('00','9f','ff'))), tuple(map(1,('ec','2f','4B')))
cdict = {'red':   ((0,blue[0],blue[0]),
                   (1,red[0],red[0])),
         'green': ((0, blue[1], blue[1]),
                   (1, red[1], red[1])),
         'blue': ((0, blue[2], blue[2]),
                  (1, red[2], red[2]))}

cmap = matplotlib.color.LinearSegmentedColormap('evening_sunshine',cdict)
for (x,y),event,ob in zip(tsne_embedding,events,obs):
    y *= -1
    zs, ys, phis = event[:,:3].T
    ys, phis, zs = ys*Rx, phis*Ry, zs/zs.sum()
    lw = 0.5
    c = cmap(ob,obs_max)
    
    plt.scatter(x+ys, y+phis, s=zf*zs, marker='.', lw=0, color=c)
    plt.gca().add_patch(matplotlib.patches.Ellipse((x,y), 2*Rx, 2*Ry, fill=None,
                                                   edgecolor=c, lw=lw))

plt.xlim(xlim); plt.ylim(ylim)
plt.xlabel('t-SNEE Manifold Dimension 1'); plt.ylabel('t-SNE Manifold Dimension 2')
plt.xticks([]); plt.yticks([])

im = plt.imshow([[]], cmap=cmap, vmin=0, vmax=1)
cmap = plt.colorbar(im, fraction=0.03175, pad=0.025, aspect=30)
cmap.set_label(r'$W$ Jet Angularity Fractile CHANGE', rotation=270, labelpad=15)

plt.savefig("TSNE.pdf", bbox_inches='tight')
plt.show()    
