import h5py
import argparse
import numpy as np
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser("plotCorrDim")
parser.add_argument('--infilesEMD', nargs='+', type=str)
parser.add_argument('--label', type=str)
myargs = parser.parse_args()

icolor = 1

colors = ['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080', '#ffffff', '#000000']

for file in myargs.infilesEMD:
    label = file.split('/')[-1].split('_')[0].replace('AntiKt10CSSKUFO','')
    if(label == ''):
        label = 'Ungroomed'

    h5f = h5py.File(file,'r')
    emds = h5f['emds']

    bins = 10**np.linspace(-2, 0, 60)
    reg = 10**-30
    midbins = (bins[:-1] + bins[1:])/2
    dmidbins = np.log(midbins[1:]) - np.log(midbins[:-1]) + reg
    midbins2 = (midbins[:-1] + midbins[1:])/2

    dims = []
    uemds = np.triu(emds)
    counts = np.cumsum(np.histogram(uemds[uemds > 0], bins=bins)[0])
    dims.append((np.log(counts[1:] + reg) - np.log(counts[:-1] + reg))/dmidbins)

    plt.plot(midbins2, dims[0], '-', color=colors[icolor], label=label)
    plt.legend(loc='center right', frameon=False)
    plt.xscale('log')
    plt.xlabel('Energy Scale Q/pT'); plt.ylabel('Correlation Dimension')
    plt.xlim(0.02, 1); plt.ylim(0, 5)

    icolor += 1

plt.savefig(myargs.label+'_corrDim.png')
