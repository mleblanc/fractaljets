import h5py
import argparse
import numpy as np
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser("plotEMDdist")
parser.add_argument('--infilesEMD', nargs='+', type=str)
parser.add_argument('--label', type=str)
myargs = parser.parse_args()

icolor = 1

colors = ['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080', '#ffffff', '#000000']

fig_size = plt.rcParams["figure.figsize"]
fig_size[0] = 12
fig_size[1] = 10
plt.rcParams["figure.figsize"] = fig_size

for file in myargs.infilesEMD:
    label = file.split('/')[-1].split('_')[0].replace('AntiKt10CSSKUFO','')
    if(label == ''):
        label = 'Ungroomed'

    print(label)

    h5f = h5py.File(file,'r')
    emds = h5f['emds']

    allEMDs  = [j for sub in emds for j in sub]

    plt.hist(allEMDs, bins=75, range=(0.0,0.75), histtype='step', label=label)
    plt.legend(loc='center right', frameon=False, fontsize="x-large")
    plt.xlabel('EMD (normalized)', fontsize="x-large");


    icolor += 1


plt.savefig(myargs.label+'_EMDdist.png')
