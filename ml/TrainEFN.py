import math
import os
import re
import numpy as np
import h5py
import argparse
import glob

# energyflow imports
import energyflow as ef
from energyflow.archs import EFN
from energyflow.datasets import qg_jets
from energyflow.utils import data_split, remap_pids, to_categorical

# attempt to import sklearn
try:
    from sklearn.metrics import roc_auc_score, roc_curve
except:
    print('please install scikit-learn in order to make ROC curves')
    roc_curve = False

# attempt to import matplotlib
try:
    import matplotlib.pyplot as plt
except:
    print('please install matploltib in order to make plots')
    plt = False

parser = argparse.ArgumentParser("MLBJESJER")
parser.add_argument('--infilesA', type=str, default="infile.A.root")
parser.add_argument('--infilesB', type=str, default="infile.B.root")
parser.add_argument('--titleA', type=str, default="sample A")
parser.add_argument('--titleB', type=str, default="sample B")
parser.add_argument('--epochs', type=int, default=50)
parser.add_argument('--batch_size', type=int, default=500)
parser.add_argument('--latent_dim', type=int, default=7)
myargs = parser.parse_args()

in_file_A = myargs.infilesA
in_file_B = myargs.infilesB

jets_A = []
files_A = glob.glob(myargs.infilesA)
print(files_A)
for fA in files_A:
    h5f_A = h5py.File(fA,'r')
    if("Sherpa" in myargs.titleA): jets_A.extend(h5f_A['jet_array'][0:195704]) # crude downsampling
    elif("Herwig" in myargs.titleA): jets_A.extend(h5f_A['jet_array'][0:(702568+101903)])
    else: jets_A.extend(h5f_A['jet_array'])
    h5f_A.close()

print("Jets A length:",len(jets_A))
    
jets_B = []
files_B = glob.glob(myargs.infilesB)
print(files_B)
for fB in files_B:
    h5f_B = h5py.File(fB,'r')
    if("Sherpa" in myargs.titleB): jets_B.extend(h5f_B['jet_array'][0:(39140+4900)]) # crude downsampling
    elif("Herwig" in myargs.titleB): jets_B.extend(h5f_B['jet_array'])
    else: jets_B.extend(h5f_B['jet_array'])
    h5f_B.close()

print("Jets B length:",len(jets_B))
    
jet_array_A = np.asarray(jets_A)
jet_array_B = np.asarray(jets_B)

print(jet_array_A.shape)
print(jet_array_B.shape)

# HACK HACK HACK MLB HACK
jet_array_A = np.squeeze(jet_array_A)
jet_array_B = np.squeeze(jet_array_B)

print(jet_array_A.shape)
print(jet_array_B.shape)

# train with reco
#jet_array_A = jet_array_A[:,:,0:3]
#jet_array_B = jet_array_B[:,:,0:3]

# or, train with truth?
jet_array_A = jet_array_A[:,:,3:6]
jet_array_B = jet_array_B[:,:,3:6]

print(jet_array_A.shape)
print(jet_array_B.shape)

# Mess around with the EFN

################################### SETTINGS ###################################
# the commented values correspond to those in 1810.05165
###############################################################################

# data controls, can go up to 2000000 for full dataset
#train, val, test = 25000, 10000, 10000
# train, val, test = 1000000, 200000, 200000
#use_pids = True

# network architecture parameters
Phi_sizes, F_sizes = (100, 100, 128), (100, 100, 100)
#Phi_sizes, F_sizes = (100, 100, 256), (100, 100, 100)

# network training parameters
num_epoch = myargs.epochs
batch_size = myargs.batch_size

# prepare inputs
X = np.concatenate((jet_array_A, jet_array_B), axis=0)
print(X.shape)
y = np.concatenate((np.zeros(jet_array_A.shape[0]), 
                    np.ones(jet_array_B.shape[0])),
                   axis=0)

#Convert labels to categorical. This is a "one-hot" encoding with 0 index being background and 1 index being signal
Y = to_categorical(y, num_classes=2)
print('Shape of labels: {}\n'.format(Y.shape))
print(Y[0:3])  # Background
print(Y[-5:-1])  # Signal  assert X.shape[0] == Y.shape[0]
print(X.shape[-1])
print(Y.shape[-1])
#assert X.shape[-1] == Y.shape[-1]

# data controls, can go up to 2000000 total for full dataset
train_frac, val_frac, test_frac = 0.50, 0.20, 0.30
assert np.sum([train_frac, val_frac, test_frac]) == 1.0

# network architecture parameters
# From https://energyflow.network/docs/archs/#efn
# ppm_sizes: The sizes of the dense layers in the per-particle frontend module phi.
# The last element will be the number of latent observables that the model defines.
latent_space_size_exponent = myargs.latent_dim
ppm_sizes = (100, 100, int(2 ** latent_space_size_exponent))
dense_sizes = (100, 100, 100)

# build architecture
# efn = EFN(input_dim=X.shape[-1], ppm_sizes=ppm_sizes, dense_sizes=dense_sizes)
print("Input dim ",X.shape[-1]-1," should be 2")
efn = EFN(input_dim=X.shape[-1]-1, Phi_sizes=ppm_sizes, F_sizes=dense_sizes,
          mask_val=-99,
          latent_dropout=0.25,
          F_dropouts=0) #dense_dropouts seems broken

# do train/val/test split
#(X_train, X_val, X_test,
# Y_train, Y_val, Y_test) = data_split(X, Y, val=val_frac, test=test_frac)

# do train/val/test split
(z_train, z_val, z_test,
  p_train, p_val, p_test,
  Y_train, Y_val, Y_test) = data_split(X[:,:,0], X[:,:,1:], Y, val=val_frac, test=test_frac)

print(z_train[0],p_train[0],Y_train[0])
print(z_train[1],p_train[1],Y_train[1])

# train the model
'''
history = pfn.fit(X_train, Y_train,
                  epochs=num_epoch,
                  batch_size=batch_size,
                  validation_data=(X_val, Y_val),
                  shuffle=True,
                  verbose=1)
model = pfn.model
'''

history =efn.fit([z_train, p_train], Y_train,
                 epochs=num_epoch,
                 batch_size=batch_size,
                 shuffle=True,
                 validation_data=([z_val, p_val], Y_val),
                 verbose=1)
model=efn.model

# get predictions on test data
#preds = model.predict(X_test, batch_size=1000)
preds = efn.predict([z_test, p_test], batch_size=1000)

efn_false_positive, efn_true_positive, threshs = roc_curve(
    Y_test[:, 1], preds[:, 1])

auc = roc_auc_score(Y_test[:, 1], preds[:, 1])
print('\nEFN AUC: {AUC}\n'.format(AUC=auc))


# plt.rcParams['font.family'] = 'serif'
# plt.rcParams['figure.autolayout'] = True

######################### Save the Model #########################

 # If a model was loaded then don't resave it
gen='gen'
if('Sherpa' in myargs.titleA): gen='sherpa',
if('Herwig' in myargs.titleA): gen='herwig'
model_file_name = 'EFN_Truth_latent{latent_exp}_ppm{num_ppm_layers}_dense{num_dense_layers}_batch{batch_size}_epochs{num_epochs}_'.format(
    latent_exp=int(latent_space_size_exponent),
    num_ppm_layers=len(ppm_sizes),
    num_dense_layers=len(dense_sizes),
    batch_size=int(batch_size),
    num_epochs=int(num_epoch),
)
gen=''
if('Sherpa' in myargs.titleA): gen='sherpa'
elif('Herwig' in myargs.titleA): gen='herwig'
model_file_name+=gen
model_file_name+='.h5'
efn.model.save('models/'+model_file_name)

######################### ROC Curve Plot #########################

# plt.rcParams['font.family'] = 'serif'
# plt.rcParams['figure.autolayout'] = True

fig, axes = plt.subplots(1, 2, figsize=(10, 5))

# plot the ROC curves
axes[0].plot(efn_true_positive, 1-efn_false_positive,
             '-', color='black', label='EFN')
axes[1].semilogy(efn_true_positive, 1./efn_false_positive,
                 '-', color='black', label='EFN')

# axes labels
axes[0].set_xlabel('B Efficiency $(\epsilon_{\mathrm{sig}})$')
axes[0].set_ylabel('A Rejection $(1-\epsilon_{\mathrm{bkg}})$')

axes[1].set_xlabel('B Efficiency $(\epsilon_{\mathrm{sig}})$')
axes[1].set_ylabel('A Rejection $(1/\epsilon_{\mathrm{bkg}})$')

# axes limits
axes[0].set_xlim(0, 1)
axes[0].set_ylim(0, 1)

axes[1].set_xlim(0, 1)

# make legend and show plot
axes[0].legend(loc='best', frameon=False)
axes[1].legend(loc='best', frameon=False)

os.system('mkdir figures/'+model_file_name[:-3]+'/')
plt.savefig('figures/'+model_file_name[:-3]+'/ROC.pdf')
plt.close()

info_file = open('figures/'+model_file_name[:-3]+'/info.txt',"a") 
info_file.write(str(auc))
#info_file.write(history.history)

print(history)
print(history.history)

# Plot training & validation loss values
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig('figures/'+model_file_name[:-3]+'/val_loss.pdf')
plt.close()

# Plot training & validation accuracy values
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('Model accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig('figures/'+model_file_name[:-3]+'/val_acc.pdf')
plt.close()

# ---

##################################################################

# Weird filter plots

X,Y,filters = efn.eval_filters(0.4, n=100, prune=False)
print(filters)
print(len(filters))

# plot filters
from matplotlib.pyplot import cm
n=128
R=0.4
color=iter(cm.rainbow(np.linspace(0,1,n)))
if("Sherpa" in myargs.titleA): color=iter(cm.inferno(np.linspace(0,1,n)))
if("Herwig" in myargs.titleA): color=iter(cm.viridis(np.linspace(0,1,n)))

for i,z in enumerate(filters):
    if("Sherpa" in myargs.titleA): plt.imshow(z, cmap='inferno')
    if("Herwig" in myargs.titleA): plt.imshow(z, cmap='viridis')
    axes[1].set_xticks(np.linspace(-R, R, 5))
    axes[1].set_yticks(np.linspace(-R, R, 5))
    axes[1].set_xticklabels(['-R', '-R/2', '0', 'R/2', 'R'])
    axes[1].set_yticklabels(['-R', '-R/2', '0', 'R/2', 'R'])
    axes[1].set_xlabel('Translated Rapidity y')
    axes[1].set_ylabel('Translated Azimuthal Angle phi')
    axes[1].set_title('Energy Flow Network Latent Space', fontdict={'fontsize': 10})
    plt.savefig('figures/'+model_file_name[:-3]+'/filters_'+str(i)+'.png')
    plt.close()

fig, axs = plt.subplots(12, 12)
for i in range(0,12):
    for j in range(0,12):
        if((12*i+j) < 128):
            if("Sherpa" in myargs.titleA): axs[i,j].imshow(filters[12*i+j], cmap='inferno', interpolation='nearest')
            if("Herwig" in myargs.titleA): axs[i,j].imshow(filters[12*i+j], cmap='viridis', interpolation='nearest')
        axs[i,j].axis('off')
fig.set_size_inches(18.5, 10.5)
plt.savefig('figures/'+model_file_name[:-3]+'/filters.png')
plt.close()
            
for i in range(0,n):
    c=next(color)
    if(i<len(filters)): plt.contour(filters[i], 2, colors=[c])
    plt.axis('off')
fig = plt.gcf()
fig.set_size_inches(10.5, 10.5)
fig.savefig('figures/'+model_file_name[:-3]+'/filters_groovy.png', dpi=200)
plt.close()
