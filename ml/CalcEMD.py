import math
import os
import re
import numpy as np
import h5py
import argparse
import glob

# energyflow imports
import energyflow as ef
#from energyflow.archs import EFN
#from energyflow.datasets import qg_jets
from energyflow.utils import data_split, remap_pids, to_categorical

# attempt to import matplotlib
try:
    import matplotlib.pyplot as plt
except:
    print('please install matploltib in order to make plots')
    plt = False

parser = argparse.ArgumentParser("CalcEMD")
parser.add_argument('--infiles', nargs='+')
myargs = parser.parse_args()

jets = []
#files = glob.glob(myargs.infiles)
files = myargs.infiles
print(files)
for f in files:
    h5f = h5py.File(f,'r')
    #if("Sherpa" in myargs.titleA): jets_A.extend(h5f_A['jet_array'][0:195704]) # crude downsampling
    #elif("Herwig" in myargs.titleA): jets_A.extend(h5f_A['jet_array'][0:(702568+101903)])
    #else:
    jets.extend(h5f['jet_array'])
    h5f.close()
    print("Jets length:",len(jets))

print("Jets length:",len(jets))

jet_array = np.asarray(jets)

print(jet_array.shape)

# HACK HACK HACK MLB HACK
jet_array = np.squeeze(jet_array)

print(jet_array.shape)

# train with reco
njets = 1000

jet_array_reco = jet_array[0:njets,:,0:3]

jet_array_truth = jet_array[0:njets,:,3:6]

recoList = jet_array_reco.tolist()
recoList = [[j for j in i if j != [-99.0, -99.0, -99.0]] for i in recoList]

trueList = jet_array_truth.tolist()
trueList = [[j for j in i if j != [-99.0, -99.0, -99.0]] for i in trueList]

#print(recoList)
#print(trueList)

# or, train with truth?
#jet_array_A = jet_array_A[:,:,3:6]

#print(jet_array.shape)

#jet_array = jet_array[0:1000]

#print(jet_array.shape)

#print(jet_array)

#jetList = jet_array.tolist()

#jetList = [[j for j in i if j != [-99.0, -99.0, -99.0]] for i in jetList]

#print(jetList)

emds = ef.emd.emds(recoList, R=1.0,  verbose=1, print_every=10000)
#emds = ef.emd.emds(recoList, trueList, R=1.0)

print(len(emds), len(emds[0]))

#print(emds)

avgEMDs = [sum(emds[i]) / len(emds[i]) for i in range(len(emds))]

allEMDs  = [j for sub in emds for j in sub]

#print(avgEMDs)

plt.hist(avgEMDs, bins=100, range=(0.0,1.0))

plt.savefig('avgEMDs_reco.png')

plt.hist(allEMDs, bins=100, range=(0.0,1.0))

plt.savefig('allEMDs_reco.png')

plt.imshow(emds);
plt.colorbar()
plt.savefig('EMD_matrix.png')

plt.close()

pts = [i[0] for i in recoList[0]]
ys = [i[1] for i in recoList[0]]
phis = [i[2] for i in recoList[0]]
plt.scatter(ys, phis, marker='o', s=[1000*pts[i] for i in range(len(pts))], color='red')
R=0.4
plt.xlim(-R, R); plt.ylim(-R, R)
plt.xlabel('Rapidity'); plt.ylabel('Azimuthal Angle')
plt.savefig('jet_recoJet.png')

ntracks = [len(j) for j in recoList for j in recoList]

print(len(allEMDs), len(ntracks))

xedges = np.linspace(0, 0.5, 100)
yedges = np.linspace(0, 30, 30)
pdf_xy, _, _ = np.histogram2d(x=allEMDs, y=ntracks, bins=(xedges, yedges))
fig, ax = plt.subplots(figsize=(6, 5))

xmesh, ymesh = np.meshgrid(xedges, yedges)
pc = ax.pcolormesh(xmesh, ymesh, pdf_xy.T)
fig.colorbar(pc, ax=ax, label='Counts')

#plt.hist2d(allEMDs, ntracks, bins=(100,50))#, range=((0.0,1.0),(0,50)) )

plt.savefig('allEMDs_reco_v_ntrack.png')
