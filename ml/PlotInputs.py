import math
import re
#import ROOT
import numpy as np
from numpy import stack, vstack, hstack, hsplit
import h5py
import argparse
import glob
#import JetPlots

# attempt to import matplotlib
try:
    import matplotlib
    import matplotlib.pyplot as plt
except:
    print('please install matploltib in order to make plots')
    plt = False

parser = argparse.ArgumentParser("MLBJESJER")
parser.add_argument('--infilesA', type=str, default="infile.A.root")
parser.add_argument('--infilesB', type=str, default="infile.B.root")
parser.add_argument('--titleA', type=str, default="sample A")
parser.add_argument('--titleB', type=str, default="sample B")
myargs = parser.parse_args()

jets_A = []
files_A = glob.glob(myargs.infilesA)
print(files_A)
for fA in files_A:
    h5f_A = h5py.File(fA,'r')
    jets_A.extend(h5f_A['jet_array'])
    h5f_A.close()
    
jets_B = []
files_B = glob.glob(myargs.infilesB)
print(files_B)
for fB in files_B:
    h5f_B = h5py.File(fB,'r')
    jets_B.extend(h5f_B['jet_array'])
    h5f_B.close()
    
jet_array_A = np.asarray(jets_A)
jet_array_B = np.asarray(jets_B)

pts_A = jet_array_A[:,:,0]
pt_masked_A=jet_array_A[pts_A>0]
pt_masked_A=pt_masked_A[:,0]
pts_B = jet_array_B[:,:,0]
pt_masked_B=jet_array_B[pts_B>0]
pt_masked_B=pt_masked_B[:,0]

eta_masked_A=jet_array_A[pts_A>0]
eta_masked_A=eta_masked_A[:,1]
eta_masked_B=jet_array_B[pts_B>0]
eta_masked_B=eta_masked_B[:,1]

phi_masked_A=jet_array_A[pts_A>0]
phi_masked_A=phi_masked_A[:,2]
phi_masked_B=jet_array_B[pts_B>0]
phi_masked_B=phi_masked_B[:,2]

tpts_A = jet_array_A[:,:,3]
tpt_masked_A=jet_array_A[tpts_A>0]
tpt_masked_A=tpt_masked_A[:,3]
tpts_B = jet_array_B[:,:,3]
tpt_masked_B=jet_array_B[tpts_B>0]
tpt_masked_B=tpt_masked_B[:,3]

teta_masked_A=jet_array_A[tpts_A>0]
teta_masked_A=teta_masked_A[:,4]
teta_masked_B=jet_array_B[tpts_B>0]
teta_masked_B=teta_masked_B[:,4]

tphi_masked_A=jet_array_A[tpts_A>0]
tphi_masked_A=tphi_masked_A[:,5]
tphi_masked_B=jet_array_B[tpts_B>0]
tphi_masked_B=tphi_masked_B[:,5]

# should be the pT 
plt.hist(pt_masked_A,bins=25,range=(0,1), density=True, histtype='step',label=myargs.titleA)
plt.hist(pt_masked_B,bins=25,range=(0,1), density=True, histtype='step',label=myargs.titleB)
plt.yscale('log')
plt.legend(loc='upper right')
plt.savefig("figures/trk_pt.png")
plt.close()

# should be the eta
plt.hist(eta_masked_A,bins=40,range=(-0.4,0.4), density=True, histtype='step',label=myargs.titleA)
plt.hist(eta_masked_B,bins=40,range=(-0.4,0.4), density=True, histtype='step',label=myargs.titleB)
plt.yscale('log')
plt.legend(loc='upper right')
plt.savefig("figures/trk_eta.png")
plt.close()

# should be the phi
plt.hist(phi_masked_A,bins=40,range=(-0.4,0.4), density=True, histtype='step',label=myargs.titleA)
plt.hist(phi_masked_B,bins=40,range=(-0.4,0.4), density=True, histtype='step',label=myargs.titleB)
plt.yscale('log')
plt.legend(loc='upper right')
plt.savefig("figures/trk_phi.png")
plt.close()

# should be the truth pT
plt.hist(tpt_masked_A,bins=25,range=(0,1), density=True, histtype='step',label=myargs.titleA+" (true)")
plt.hist(tpt_masked_B,bins=25,range=(0,1), density=True, histtype='step',label=myargs.titleB+" (true)")
plt.yscale('log')
plt.legend(loc='upper right')
plt.savefig("figures/trk_tpt.png")
plt.close()

# should be the truth eta
plt.hist(teta_masked_A,bins=40,range=(-0.4,0.4), density=True, histtype='step',label=myargs.titleA+" (true)")
plt.hist(teta_masked_B,bins=40,range=(-0.4,0.4), density=True, histtype='step',label=myargs.titleB+" (true)")
plt.yscale('log')
plt.legend(loc='upper right')
plt.savefig("figures/trk_teta.png")
plt.close()

# should be the truth phi
plt.hist(tphi_masked_A,bins=40,range=(-0.4,0.4), density=True, histtype='step',label=myargs.titleA+" (true)")
plt.hist(tphi_masked_B,bins=40,range=(-0.4,0.4), density=True, histtype='step',label=myargs.titleB+" (true)")
plt.yscale('log')
plt.legend(loc='upper right')
plt.savefig("figures/trk_tphi.png")
plt.close()

# should be the ntrk
print(len(phi_masked_A))
print(phi_masked_A.shape)
plt.hist(len(phi_masked_A),bins=55,range=(0.,55), density=True, histtype='step',label=myargs.titleA)
plt.hist(len(phi_masked_B),bins=55,range=(0.,55), density=True, histtype='step',label=myargs.titleB)
plt.yscale('log')
plt.legend(loc='upper right')
plt.savefig("figures/trk_nTrk.png")
plt.close()

from matplotlib.colors import LogNorm

# it's important to get the colours right.
# pythia is bluish
# sherpa is a warm colour
# herwig has gotta be green yo

colour_scheme = 'viridis'
if('Pythia' in myargs.titleA): 
    out_dir = 'pythia'
    colour_scheme = 'cividis'
if('Sherpa' in myargs.titleA): 
    out_dir = 'sherpa'
    colour_scheme = 'magma'
if('Herwig' in myargs.titleA): 
    out_dir = 'herwig'
    colour_scheme = 'viridis'

# jet image A
h_A,xedges,yedges,im_A = plt.hist2d(eta_masked_A,phi_masked_A,bins=[10,10], range=((-0.4,0.4),(-0.4,0.4)), density=True, cmap=colour_scheme, norm=LogNorm(),weights=pt_masked_A)
plt.savefig("figures/trk_image_A.png")
plt.close()

# jet image B
h_B,xedges,yedges,im_B = plt.hist2d(eta_masked_B,phi_masked_B,bins=[10,10], range=((-0.4,0.4),(-0.4,0.4)), density=True, cmap=colour_scheme, norm=LogNorm(),weights=pt_masked_B)
plt.savefig("figures/trk_image_B.png")
plt.close()

# jet image A/B
d_rat_AB = np.divide(h_A,h_B,where=(h_B>0))
d_rat_AB[d_rat_AB==np.inf]=0
rat = plt.imshow(d_rat_AB, cmap=colour_scheme, vmin=0.66, vmax=1.33, norm=matplotlib.colors.Normalize(vmin=0.66,vmax=1.33))
plt.title(myargs.titleA+"/"+myargs.titleB)
plt.xlabel('eta bin')
plt.ylabel('phi bin')
cb = plt.colorbar()
cb.set_label('pt density ratio')
plt.savefig('figures/trk_image_rat.png')
plt.close()

# truth

# jet image A
h_A,xedges,yedges,im_A = plt.hist2d(teta_masked_A,tphi_masked_A,bins=[10,10], range=((-0.4,0.4),(-0.4,0.4)), density=True, cmap=colour_scheme, norm=LogNorm(),weights=tpt_masked_A)
plt.savefig("figures/trk_image_A_true.png")
plt.close()

# jet image B
h_B,xedges,yedges,im_B = plt.hist2d(teta_masked_B,tphi_masked_B,bins=[10,10], range=((-0.4,0.4),(-0.4,0.4)), density=True, cmap=colour_scheme, norm=LogNorm(),weights=tpt_masked_B)
plt.savefig("figures/trk_image_B_true.png")
plt.close()

# jet image A/B
d_rat_AB = np.divide(h_A,h_B,where=(h_B>0))
d_rat_AB[d_rat_AB==np.inf]=0
rat = plt.imshow(d_rat_AB, cmap=colour_scheme, vmin=0.66, vmax=1.33, norm=matplotlib.colors.Normalize(vmin=0.66,vmax=1.33))
plt.title(myargs.titleA+"/"+myargs.titleB)
plt.xlabel('truth eta bin')
plt.ylabel('truth phi bin')
cb = plt.colorbar()
cb.set_label('truth pt density ratio')
plt.savefig('figures/trk_image_rat_true.png')
plt.close()
