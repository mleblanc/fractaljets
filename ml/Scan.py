import os

sims = [
    #['h5/v10/*19614574*.h5','h5/v10/*19614570*.h5','Sherpa \(Lund\)','Sherpa \(AHADIC\)'],
    ['h5/v11/*19614578*.h5','h5/v11/*19614581*.h5','Herwig \(Dipole\)','Herwig \(Ang. ord.\)']
]

latent_dims = [7]
epochs = [10]
batch_sizes = [10000]

for s in sims:
    
    cmd = 'python fractaljets/ml/PlotInputs.py --infilesA \''+s[0]+'\' --infilesB \''+s[1]+'\' --titleA \''+s[2]+'\' --titleB \''+s[3]+'\''
    print(cmd)
    #os.system(cmd)
    
    if('Sherpa' in s[2]): os.system('mv figures/*.png figures/sherpa/')
    elif('Herwig' in s[2]): os.system('mv figures/*.png figures/herwig/')    
    
    for l in latent_dims:
        for e in epochs:
            for b in batch_sizes:
                cmd_train = 'python fractaljets/ml/TrainEFN.py --infilesA \''+s[0]+'\' --infilesB \''+s[1]+'\' --titleA \''+s[2]+'\' --titleB \''+s[3]+'\' --epochs '+str(e)+' --batch_size '+str(b)+' --latent_dim '+str(l)+' '
                print(cmd_train)
                #os.system(cmd_train)                
                cmd_eval = 'python fractaljets/ml/Eval.py --infilesA \''+s[0]+'\' --infilesB \''+s[1]+'\' --titleA \''+s[2]+'\' --titleB \''+s[3]+'\' --epochs '+str(e)+' --batch_size '+str(b)+' --latent_dim '+str(l)+' '
                print(cmd_eval)
                #os.system(cmd_eval)
