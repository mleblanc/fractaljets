import os

truth = 'AntiKt10Truth'

inputList = ['AntiKt10CSSKUFO',
             'AntiKt10OrigLCTopo']

groomList = ['',
             'TrimmedPtFrac5SmallR20',
             'PrunedZCut15RCut25',
             'SoftDropZCut10Beta0',
             'SoftDropZCut10Beta100',
             'BottomUpSoftDropZCut5Beta100',
             'RecursiveSoftDropZCut5Beta100N-100',
             'RecursiveSoftDropZCut5Beta100N100',
             'RecursiveSoftDropZCut5Beta100N300']

for i in inputList:
    for g1 in groomList:
        for g2 in groomList:            
                cmd  = "python src/fractaljets/ml/EMDchunk.py"
                cmd += " --infile1 HFSF/"+i+g1+".h5 --label1 "+i+g1 
                if(g1!=g2):
                    cmd += " --infile2 HFSF/"+i+g2+".h5 --label2 "+i+g2
                cmd += " --chunkSize 1000 --startJet1 0 --startJet2 0"
                print(cmd)
                os.system(cmd)
