import numpy as np
import h5py
import argparse

def getChunk(label1, startJet1, startJet2, label2 = "", chunkSize=1000):
    if label2 == "":
        label2 = label1
    print(label2)
    print(label1+'_'+str(startJet1)+'_'+label2+'_'+str(startJet2)+'_'+str(chunkSize)+'chunk.h5')
    h5f_in = h5py.File(label1+'_'+str(startJet1)+'_'+label2+'_'+str(startJet2)+'_'+str(chunkSize)+'chunk.h5', 'r')
    return np.matrix(h5f_in['emds'], dtype=np.float16)

parser = argparse.ArgumentParser("mergeChunk")
parser.add_argument('--label1', type=str)
parser.add_argument('--label2', type=str)
parser.add_argument('--startJet1', type=int)
parser.add_argument('--endJet1', type=int)
parser.add_argument('--startJet2', type=int)
parser.add_argument('--endJet2', type=int)
parser.add_argument('--chunkSize', type=int, default=1000)
myargs = parser.parse_args()

chunkSize = myargs.chunkSize

useTwoInputs = 1 if myargs.label2 is not None else 0

nChunks1 = int( (myargs.endJet1 - myargs.startJet1 ) / myargs.chunkSize)
nChunks2 = int( (myargs.endJet2 - myargs.startJet2 ) / myargs.chunkSize)

startJet1 = myargs.startJet1
startJet2 = myargs.startJet2

label2 = ""
if useTwoInputs:
    label2 = myargs.label2

fullMatrix = np.matrix([], dtype=np.float16)

for i in range(nChunks1):
    matrixSlab = np.matrix([], dtype=np.float16)
    for j in range(nChunks2):
        EMDmatrix = getChunk(myargs.label1, startJet1 + i*chunkSize, startJet2 + j*chunkSize, label2)
        print(EMDmatrix.shape)
        if j == 0:
            matrixSlab = EMDmatrix
        else:
            shA=np.shape(matrixSlab)
            shB=np.shape(EMDmatrix)
            rowTot=shA[0]+shB[0]
            colTot=shA[1]+shB[1]
            rowMax=np.max((shA[0],shB[0]))
            colMax=np.max((shA[1],shB[1]))
            CHorz=np.zeros((rowMax,colTot)).astype('float16')
            CHorz[0:shA[0],0:shA[1]]=matrixSlab
            CHorz[0:shB[0],shA[1]:colTot]=EMDmatrix
            matrixSlab = CHorz
    if i == 0:
        fullMatrix = matrixSlab
    else:
        shA=np.shape(fullMatrix)
        shB=np.shape(matrixSlab)
        rowTot=shA[0]+shB[0]
        colTot=shA[1]+shB[1]
        rowMax=np.max((shA[0],shB[0]))
        colMax=np.max((shA[1],shB[1]))
        CVert=np.zeros((rowTot,colMax)).astype('int')
        CVert[0:shA[0],0:shA[1]]=fullMatrix
        CVert[shA[0]:rowTot,0:shB[1]]=matrixSlab
        fullMatrix = CVert


print(fullMatrix.shape)


print(nChunks1, nChunks2)

#h5f_out = h5py.File(label1+'_'+label2+'_emdMerge.h5', 'w')
#h5f_out.create_dataset('emds', data=emds)
#h5f_out.close()
