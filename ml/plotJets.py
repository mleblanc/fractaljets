import h5py
import argparse
import numpy as np
import matplotlib.pyplot as plt

def getJets(file, startJet, chunkSize, reco = 1):
    h5f = h5py.File(file,'r')
    jets = []
    jets.extend(h5f['jet_array'][startJet:startJet+chunkSize])

    jet_array = np.asarray(jets)
    jet_array = np.squeeze(jet_array)
    jet_array = jet_array[:,:,0:3] if reco else jet_array[:,:,3:6]
    jetList = jet_array.tolist()
    jetList = [[j for j in i if j != [-99.0, -99.0, -99.0]] for i in jetList]
    return jetList

def plotJet(jetList, color):
    pts = [i[0] for i in jetList]
    ys = [i[1] for i in jetList]
    phis = [i[2] for i in jetList]
    plt.scatter(ys, phis, marker='o', s=[1000*pts[i] for i in range(len(pts))], color=color)
    R=1.0
    plt.xlim(-R, R); plt.ylim(-R, R)
    plt.xlabel('Rapidity'); plt.ylabel('Azimuthal Angle')


parser = argparse.ArgumentParser("plotJets")
parser.add_argument('--nJets', type=int, default=3)
parser.add_argument('--infileJets', type=str)
parser.add_argument('--startJet', type=int, default=0)
parser.add_argument('--chunkSize', type=int, default=10000)
myargs = parser.parse_args()

jets = getJets(myargs.infileJets, myargs.startJet, myargs.chunkSize)

colors = ['red', 'blue', 'green']

for i in range(myargs.nJets):
    plt.close()
    plotJet(jets[i], colors[i])
    plt.savefig('jet_'+str(i)+'.png')
