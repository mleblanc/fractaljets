/*
  HistoAlgo.cxx

  Prototype code by M. LeBlanc (Arizona) <matt.leblanc@cern.ch>
  for 2019-2020 JSS studies.

  This one makes the histograms.
*/

#include <AsgTools/MessageCheck.h>
#include <fractaljets/HistoAlgo.h>

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>
#include <SampleHandler/DiskOutputXRD.h>

#include "TFile.h"
#include "TSystem.h"
#include "TLorentzVector.h"

// this is needed to distribute the algorithm to the workers
ClassImp(HistoAlgo)


HistoAlgo :: HistoAlgo ()
{
}

EL::StatusCode HistoAlgo :: setupJob (EL::Job& job)
{
  TString out = "out_"+m_label;
  EL::OutputStream outStream (out.Data());
  std::cout << "Adding output stream, " << out.Data() << std::endl;;
  //EL::OutputStream outStream (new SH::DiskOutputXRD (TString("root://faxbox.usaltas.org/"+m_outDir+"/"+out).Data()));
  job.outputAdd (outStream);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HistoAlgo :: histInitialize ()
{
  TString out = "out_"+m_label;
  std::cout << "output stream, " << out << std::endl;
  TFile *file = wk()->getOutputFile(out.Data());
  
  //TH1F* h1_mu = new TH1F("mu_"+m_label,"mu_"+m_label,70,0,70.); h1_mu->SetDirectory(file);
  //TH1F* h1_npv = new TH1F("npv_"+m_label,"npv_"+m_label,70,0,70.); h1_npv->SetDirectory(file);  

  h1_run = new TH1F("run_"+m_label,"run_"+m_label,88031,276261.5,364292.5); h1_run->SetDirectory(file);
  h1_mu = new TH1F("mu_"+m_label,"mu_"+m_label,70,0,70.); h1_mu->SetDirectory(file);
  h1_npv = new TH1F("npv_"+m_label,"npv_"+m_label,70,0,70.); h1_npv->SetDirectory(file);

  h1_tjetN = new TH1F("h1_tjetN_"+m_label,"h1_tjetN_"+m_label,5,0,5); h1_tjetN->SetDirectory(file);
  h1_tjetCount = new TH1F("h1_tjetCount_"+m_label,"h1_tjetCount_"+m_label,1,1,2); h1_tjetCount->SetDirectory(file);
  h1_tjetBalance = new TH1F("h1_tjetBalance_"+m_label,"h1_tjetBalance_"+m_label,20,0,1); h1_tjetBalance->SetDirectory(file);
  h1_tjetPt = new TH1F("h1_tjetPt_"+m_label,"h1_tjetPt_"+m_label,200,0,5000); h1_tjetPt->SetDirectory(file);
  h1_tjetEta = new TH1F("h1_tjetEta_"+m_label,"h1_tjetEta_"+m_label,35,-3.5,3.5); h1_tjetEta->SetDirectory(file);
  h1_tjetPhi = new TH1F("h1_tjetPhi_"+m_label,"h1_tjetPhi_"+m_label,63,0,6.3); h1_tjetPhi->SetDirectory(file);
  h1_tjetE = new TH1F("h1_tjetE_"+m_label,"h1_tjetE_"+m_label,200,0,5000); h1_tjetE->SetDirectory(file);  
  
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HistoAlgo :: fileExecute ()
{
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HistoAlgo :: changeInput (bool firstFile)
{
  //if((m_doJESJER && m_JESJER_idx==0)||(!m_doJESJER))
  // {
  TTree* t = wk()->tree();

  if(firstFile)
    {
      std::cout << "Input tree info ..." << std::endl;
      t->Print();
    }

  if(!m_isData)
    {
      t->SetBranchAddress("DSID",&m_DSID);
      t->SetBranchAddress("mcWeight",&m_mcWeight);
      //t->SetBranchAddress("showerWeights",&m_showerWeights);
      t->SetBranchAddress("weight_pileup",&m_weight_pileup);
      t->SetBranchAddress("weight_pileup_up",&m_weight_pileup_up);
      t->SetBranchAddress("weight_pileup_down",&m_weight_pileup_down);
    }

  t->SetBranchAddress("mu",&m_mu);
  t->SetBranchAddress("npv",&m_npv);
  t->SetBranchAddress("RunNumber",&m_RunNumber);
  //if(m_isData) t->SetBranchAddress("passedTriggers",&m_PassTrigger);

  if(!m_isData)
    {
      t->SetBranchAddress("tjetN",&m_tjetN);
      t->SetBranchAddress("tjetPt",&m_tjetPt);
      t->SetBranchAddress("tjetEta",&m_tjetEta);
      t->SetBranchAddress("tjetPhi",&m_tjetPhi);
      t->SetBranchAddress("tjetE",&m_tjetE);
    }

  t->SetBranchAddress("rjetN",&m_rjetN);
  //t->SetBranchAddress("rjetPt",&m_rjetPt);
  //t->SetBranchAddress("rjetEta",&m_rjetEta);
  //t->SetBranchAddress("rjetPhi",&m_rjetPhi);
  //t->SetBranchAddress("rjetE",&m_rjetE);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HistoAlgo :: initialize ()
{
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode HistoAlgo :: execute ()
{
  wk()->tree()->GetEntry (wk()->treeEntry());

  static int count = 0;
  bool debug=false;
  if(count==0)
    {
      debug = true;
      std::cout << "HistoAlgo :: execute() BEGIN" << std::endl;
    }
  count++;
  if(count % m_MessageFrequency == 0) debug = true; // Defaults to print every 1000 events

  if(debug) std::cout << "HistoAlgo :: execute()\tProcessing event " << count << ". "  << std::endl;
  if(debug) std::cout << "HistoAlgo :: execute()\tRunNumber " << m_RunNumber   << std::endl;

  Float_t weight = 1.0;

  if(!m_isData)
    {
      // Extract campaign automatically from Run Number
      std::string campaign = "";
     
      switch(m_RunNumber)
	{
	case 284500 :
	  campaign="MC16a";
	  break;
	case 300000 :
	  campaign="MC16d";
	  break;
	  // This should be switched to mc16f once it is available.
	case 310000 :
	  campaign="MC16e";
	  break;
	default :
	  ANA_MSG_ERROR( "Could not determine mc campaign from run number!! Aborting." );
	  return StatusCode::FAILURE;
	  break;
	}      

      Float_t mc_weight = 0;
      if(m_doShowerWeights)// || campaign=="MC16e")
        {
          //mc_weight = m_showerWeights->at(m_ShowerWeights_idx);
	  //if(debug) std::cout << "Shower weights activated!\tIDX = " << m_ShowerWeights_idx << "\tWeight:\t" << mc_weight << std::endl;
        }
      else mc_weight = m_mcWeight;
      Int_t dsid = m_DSID;

      if(debug) std::cout << "MC\tDSID\t" << dsid << "\tMCWeight\t" << mc_weight << "\tcampaign:\t" << campaign << std::endl;

      weight *= mc_weight;

      float xs = 1.;

      // R21 mc16a pythia
      if(campaign=="MC16a")
	{
	  //if(dsid==361020) xs=(78420000000000./15527000.); // xSec*FE/evnts in fb !!!!!
	  //if(dsid==361021) xs=(78420000000000./15997000.);
	  //if(dsid==361022) xs=(2433200000000.*3.3423E-04/15983000.);
	  //if(dsid==361023) xs=(2433200000000.*3.3423E-04/15983000.); // jz3
	  if(dsid==361024) xs=(254630000.*5.3138E-04/15925500.); // jz4
	  if(dsid==361025) xs=(4553500.*9.2409E-04/15993500);    // jz5
	  if(dsid==361026) xs=(257530.000*9.4092E-04/17834000.); // jz6
	  if(dsid==361027) xs=(16215.*3.9280E-04/15118500.);     // jz7
	  if(dsid==361028) xs=(625.030*1.0176E-02/15997000.);    // jz8
	  if(dsid==361029) xs=(19.639*1.2076E-02/(14552500.-80000.));     // jz9
	  if(dsid==361030) xs=(1.196*5.9087E-03/15998000.);      // jz10
	  //if(dsid==361031) xs=(0.042*2.6761E-03/15973000.); // jz11
	  //if(dsid==361032) xs=(0.0010367*4.2592E-04/15070000.); // jz12
	}
      
      // mc16d Pythia 8
      if(campaign=="MC16d")
	{
	  //if(dsid==361020) xs=(78420000000000./15987000.); // xSec*FE/evnts in fb !!!!!
	  //if(dsid==361021) xs=(78420000000000./15997000.);
	  //if(dsid==361022) xs=(2433200000000.*3.3423E-04/15981000.);
	  //if(dsid==361023) xs=(2433200000000.*3.3423E-04/15878500.);
	  if(dsid==361024) xs=(254630000.*5.3138E-04/15974500.); // jz4
	  if(dsid==361025) xs=(4553500.*9.2409E-04/15991500.); // jz5
	  if(dsid==361026) xs=(257530.000*9.4092E-04/17880400.);
	  if(dsid==361027) xs=(16215.*3.9280E-04/15116500.);
	  if(dsid==361028) xs=(625.030*1.0176E-02/15987000.);
	  if(dsid==361029) xs=(19.639*1.2076E-02/14511500.);
	  if(dsid==361030) xs=(1.196*5.9087E-03/15988000.);
	  //if(dsid==361031) xs=(0.042*2.6761E-03/15993000.);
	  //if(dsid==361032) xs=(0.0010367*4.2592E-04/15640000.);
	}

      // mc16e Pythia 8 
      if(campaign=="MC16e")
        {	  	 
          //if(dsid==364701) xs=(78420000000000.  * 2.4447E-02 / 4997265.35891723);
          //if(dsid==364702) xs=(2433200000000. * 9.8699E-03 / 1842.3934256136417);
          //if(dsid==364703) xs=(2433200000000. * 1.1663E-02 / 86.0874);
          if(dsid==364704) xs=(254630000. * 1.3369E-02 / 2.88063);
          if(dsid==364705) xs=(4553500. * 1.4529E-02 / 0.11621);
          if(dsid==364706) xs=(257530. * 9.4734E-03 / 0.0193662);
          if(dsid==364707) xs=(16215. * 1.1099E-02 / 0.0053364);
          if(dsid==364708) xs=(625.030 * 1.0156E-02 / 0.00154425);
          if(dsid==364709) xs=(19.639 * 1.2057E-02 / 0.000273853);
          if(dsid==364710) xs=(1.196 * 5.8940E-03 / 3.19841e-05);
          //if(dsid==364711) xs=(0.042 * 2.6734E-03 / 1.6965e-05);
	  //if(dsid==364712) xs=(0.001 * 4.2898E-04 / 9.836e-06);
	}

      // mc16a Herwig 7
      if(campaign=="MC16a")
        {      
	  if(dsid==364443) xs=(78675000000000.*9.8033E-01/9.21105e+08); //jz0
	  if(dsid==364444) xs=(78675000000000.*1.9474E-02/1.45719e+09); //jz1
	  if(dsid==364445) xs=(1360500000000.*1.3265E-02/30735); //jz2
	  if(dsid==364446) xs=(18242000000.*1.3280E-02/1.65852e+06); //jz3
	  if(dsid==364447) xs=(193730000.*1.3879E-02/1.95699e+06); //jz4
	  if(dsid==364448) xs=(3507900.*1.4149E-02/1.99443e+06); //jz5
	  if(dsid==364449) xs=(192780.*9.0135E-03/1.98969e+06); //jz6
	  if(dsid==364450) xs=(11660.*1.0663E-02/1.98969e+06 ); //jz7  // THIS ONE IS FAKE
	  if(dsid==364451) xs=(434.04*9.7362E-03/1.9949e+06 ); //jz8
	  if(dsid==364452) xs=(13.471*1.0985E-02/498784.); //jz9	  

	  if(dsid==364453) xs=(0.818*5.4424E-03/498861.); //jz10
	  if(dsid==364454) xs=(0.029*2.9499E-03/497915.); //jz11
          //if(dsid==364455) xs=(1./497915.); //jz12 // THIS ONE IS FAKE
	}	  
      // mc16d Herwig 7
      if(campaign=="MC16d")
        {
          if(dsid==364443) xs=(78675000000000.*9.8033E-01/4.9328e+10); // jz0
          if(dsid==364444) xs=(78675000000000.*1.9474E-02/7.40359e+10); // jz1
          if(dsid==364445) xs=(1360500000000.*1.3265E-02/236223.); // jz2
          if(dsid==364446) xs=(18242000000.*1.3280E-02/2.25037e+06); // jz3
          if(dsid==364447) xs=(193730000.*1.3879E-02/2.48822e+06); // jz4
          if(dsid==364448) xs=(3507900.*1.4149E-02/2.38976e+06); // jz5
          if(dsid==364449) xs=(192780.*9.0135E-03/2.39501e+06); // jz6
          if(dsid==364450) xs=(11660.*1.0663E-02/2.44083e+06); // jz7
          if(dsid==364451) xs=(434.04*9.7362E-03/298801.); // jz8
          if(dsid==364452) xs=(13.471*1.0985E-02/647399.); // jz9 // MLB THIS ONE IS FAKE
	  
	  if(dsid==364453) xs=(0.818*5.4424E-03/646508.); //jz10
	  if(dsid==364454) xs=(0.029*2.9499E-03/643496.); //jz11
	  //if(dsid==364455) xs=(1./643496. ); //jz12 // THIS ONE IS FAKE
	}	  
      // mc16e Herwig 7
      if(campaign=="MC16e")
        {
          if(dsid==364443) xs=(78675000000000.*9.8033E-01/1.);
          if(dsid==364444) xs=(78675000000000.*1.9474E-02/1.);
          if(dsid==364445) xs=(1360500000000.*1.3265E-02/1.);
          if(dsid==364446) xs=(18242000000.*1.3280E-02/1.);
          if(dsid==364447) xs=(193730000.*1.3879E-02/1.);
          if(dsid==364448) xs=(3507900.*1.4149E-02/1.);
          if(dsid==364449) xs=(192780.*9.0135E-03/1.);
          if(dsid==364450) xs=(11660.*1.0663E-02/1.);
          if(dsid==364451) xs=(434.04*9.7362E-03/1.);
          if(dsid==364452) xs=(13.471*1.0985E-02/1.);
	}

      // mc16a Sherpa 
      if(campaign=="MC16a")
	{
	  //if(dsid==426132) xs=(107300000000.0*1.4175E-01/1.);
	  //if(dsid==426133) xs=(13075000000.0*1.8541E-02/6.64007e+06);
	  if(dsid==426134) xs=(96079000.0*2.7790E-02/7.95119e+06);
	  if(dsid==426135) xs=(2725000.0*1.8421E-02/7.9677e+06);
	  if(dsid==426136) xs=(208620.0*8.7435E-03/1.98983e+06);
	  if(dsid==426137) xs=(43732.0*3.1001E-03/1.9557e+06);
	  if(dsid==426138) xs=(333.72*1.4573E-02/1.9902e+06); 
	  if(dsid==426139) xs=(58.948*3.0994E-03/1.97696e+06);
	  if(dsid==426140) xs=(5.573*9.5787E-04/1.91172e+06);
          //if(dsid==426141) xs=(0.126*7.2054E-04/657522.);
          //if(dsid==426142) xs=(0.01*0.0004725/1.96814e+06);
	}	  
      // mc16d Sherpa
      if(campaign=="MC16d")
        {
	  //if(dsid==426132) xs=(107300000000.0*1.4175E-01/1.);
          //if(dsid==426133) xs=(13075000000.0*1.8541E-02/7.05719e+06);
          if(dsid==426134) xs=(96079000.0*2.7790E-02/7.93813e+06);
          if(dsid==426135) xs=(2725000.0*1.8421E-02/7.95665e+06);
          if(dsid==426136) xs=(208620.0*8.7435E-03/1.98843e+06);
          if(dsid==426137) xs=(43732.0*3.1001E-03/1.97827e+06);
	  if(dsid==426138) xs=(333.72*1.4573E-02/1.98867e+06); 
          if(dsid==426139) xs=(58.948*3.0994E-03/1.96802e+06);
          if(dsid==426140) xs=(5.573*9.5787E-04/1.95007e+06);
          //if(dsid==426141) xs=(0.126*7.2054E-04/1.97791e+06);
          //if(dsid==426142) xs=(0.01*0.0004725/1.97281e+06);
	}
      if(campaign=="MC16e")
        {
	  //if(dsid==426132) xs=(107300000000.0*1.4175E-01/1.);
          //if(dsid==426133) xs=(13075000000.0*1.8541E-02/1.20915e+07);
          if(dsid==426134) xs=(96079000.0*2.7790E-02/1.31576e+07);
          if(dsid==426135) xs=(2725000.0*1.8421E-02/1.31926e+07);
          if(dsid==426136) xs=(208620.0*8.7435E-03/3.16638e+06);
          if(dsid==426137) xs=(43732.0*3.1001E-03/3.24777e+06);
          if(dsid==426138) xs=(333.72*1.4573E-02/3.29696e+06);
          if(dsid==426139) xs=(58.948*3.0994E-03/3.28236e+06);
          if(dsid==426140) xs=(5.573*9.5787E-04/3.24364e+06); // FAKE
          //if(dsid==426141) xs=(0.126*7.2054E-04/3.24364e+06);
          //if(dsid==426142) xs=(0.01*0.0004725/3.09339e+06);
	}

      // std::cout << "after\t" << xs << std::endl;

      weight *= xs;

      // Integrated Lumi per Year
      // 2015:  3220.0 pb-1 +/- 2.1%
      // 2016:  32990.0 pb-1 +/- 2.1%
      // 2017:  44310.0 pb-1 +/- 2.4%
      // 2018:  58450.0 pb-1 +/- 2.0%
      // -------------------
      // Tot.: 139000.0 pb-1 +/- 1.7%
      //
      // So, weight each campaign accordingly ...

      if(campaign=="MC16a") weight *= 0.2607; // 0.02316+0.2373 
      if(campaign=="MC16d") weight *= 0.3188;
      if(campaign=="MC16e") weight *= 0.4205;

      if(m_PURW=="Off") weight *= 1.0;
      if(m_PURW=="Nominal") weight *= m_weight_pileup;
      if(m_PURW=="Up") weight *= m_weight_pileup_up;
      if(m_PURW=="Down") weight *= m_weight_pileup_down;
      
      if(m_LumiShift!=1.) weight*=m_LumiShift;      

    }
  else
    { // if it is data, check the triggers
      Bool_t pass_trigger = false;     
      /*
      for(unsigned int iTrig=0; iTrig<m_PassTrigger->size(); iTrig++)
	{
	  //std::cout << m_PassTrigger->at(iTrig) << std::endl;
	  if( (m_RunNumber>=276262) && (m_RunNumber<=284484) && (m_PassTrigger->at(iTrig)=="HLT_j360") ) 
	    {	      
	      pass_trigger = true; // 2015
	    }
	  if( (m_RunNumber>=297730) && (m_RunNumber<=311481) && (m_PassTrigger->at(iTrig)=="HLT_j380") ) 
	    {	      
	      pass_trigger = true; // 2016
	    }
          if( (m_RunNumber>=325713) && (m_RunNumber<=340453) && (m_PassTrigger->at(iTrig)=="HLT_j420") )
	    {	      
	      pass_trigger = true; // 2017
	    }
          if( (m_RunNumber>=348885) && (m_RunNumber<=364292) && (m_PassTrigger->at(iTrig)=="HLT_j420") ) 
	    {	      
	      pass_trigger = true; // 2018
	    }
      
	    }*/
      if(!pass_trigger) return EL::StatusCode::SUCCESS;
    }
  
  if(debug) std::cout << "WOOOOOO JETS RULE " << std::endl;
   
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HistoAlgo :: postExecute ()
{
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HistoAlgo :: finalize ()
{
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HistoAlgo :: histFinalize ()
{
  return EL::StatusCode::SUCCESS;
}
