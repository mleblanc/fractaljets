/*
  TrackJetTreeAlgo.cxx

  Code by M. LeBlanc (Arizona) <matt.leblanc@cern.ch>
  for 2019 measurement of jet correlation dimension.

  These trees will be passed to python scripts which compute the
  pairwise EMDs and fill the unfolding matrix, etc.
*/

#include <AsgTools/MessageCheck.h>
#include <fractaljets/TrackJetTreeAlgo.h>

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

#include "TFile.h"
#include "TSystem.h"
#include "TLorentzVector.h"

// EDM include(s):
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCore/ShallowCopy.h"
#include "AthContainers/ConstDataVector.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODEventInfo/EventInfo.h"

// jet reconstruction
//#include "JetRec/JetRecTool.h"

// fastjet user info
//#include "fractaljets/myFastJetBase.h"

TrackJetTreeAlgo :: TrackJetTreeAlgo (const std::string& name,
				      ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm (name, pSvcLocator)
{
  declareProperty( "label", m_label = "nominal",
                   "A label for the histograms" );

  declareProperty( "isData", m_isData = false,
                   "Is this data?" );

  declareProperty( "doShowerWeights", m_doShowerWeights = false,
                   "New samples with Shower Weights?" );
  
  declareProperty( "doJES", m_doJES = false,
                   "Run the JES uncertainties?" );

  declareProperty( "MessageFrequency", m_MessageFrequency = 1000,
		   "Frequency of debug messages" );
  
  declareProperty( "jetMinPt", m_jetMinPt = 600.,
                   "Minimum jet pT" );

  declareProperty( "jetMaxPt", m_jetMaxPt = 2000.,
                   "Maximum jet pT" );

  declareProperty( "jetRadius", m_jetRadius = 0.4,
                   "Jet radius parameter R" );

  declareProperty( "InputTrackCollection", m_InputTrackCollection = "InDetTrackParticles",
                   "Input track collection" );

  declareProperty( "InputJetCollection", m_InputJetCollection = "AntiKt10LCTopoJets",
                   "Input jet collection" );
  
}

TrackJetTreeAlgo :: ~TrackJetTreeAlgo()
{
  delete m_showerWeights;
  delete m_pdfWeights;

  delete m_tjet1_trk_pt;
  delete m_tjet1_trk_eta;
  delete m_tjet1_trk_y;
  delete m_tjet1_trk_phi;
  delete m_tjet1_trk_E;
  delete m_tjet1_trk_q;
  delete m_tjet1_trk_index;
  delete m_tjet2_trk_pt;
  delete m_tjet2_trk_eta;
  delete m_tjet2_trk_y;
  delete m_tjet2_trk_phi;
  delete m_tjet2_trk_E;
  delete m_tjet2_trk_q;
  delete m_tjet2_trk_index;

  delete m_rjet_N;
  delete m_rjet1_Pt;
  delete m_rjet1_Eta;
  delete m_rjet1_y;
  delete m_rjet1_Phi;
  delete m_rjet1_E;
  delete m_rjet1_Label;
  delete m_rjet1_NTrk;
  delete m_rjet1_trk_pt;
  delete m_rjet1_trk_eta;
  delete m_rjet1_trk_y;
  delete m_rjet1_trk_phi;
  delete m_rjet1_trk_E;
  delete m_rjet1_trk_truthIndex1;
  delete m_rjet1_trk_truthIndex2;

  delete m_rjet2_Pt;
  delete m_rjet2_Eta;
  delete m_rjet2_y;
  delete m_rjet2_Phi;
  delete m_rjet2_E;
  delete m_rjet2_Label;
  delete m_rjet2_NTrk;
  delete m_rjet2_trk_pt;
  delete m_rjet2_trk_eta;
  delete m_rjet2_trk_y;
  delete m_rjet2_trk_phi;
  delete m_rjet2_trk_E;
  delete m_rjet2_trk_truthIndex1;
  delete m_rjet2_trk_truthIndex2;
}

StatusCode TrackJetTreeAlgo :: initialize ()
{
  m_event = wk()->xaodEvent();

  // Output tree
  ANA_CHECK (book (TTree(TString("jets_"+m_label).Data(), "jet tree")) );
  TTree* t = tree (TString("jets_"+m_label).Data());

  // PDF weights
  nnpdf23_set = new LHAPDF::PDFSet("NNPDF23_lo_as_0119_qed");
  pdfs = nnpdf23_set->mkPDFs(); // pointers to PDF set members
  myrand_global = new TRandom3(0); //for uncerts.
    

  ANA_MSG_INFO("Initialize Jet Uncertainties Tool ");
  /*
    ANA_CHECK( ASG_MAKE_ANA_TOOL(m_JetUncertaintiesTool_handle, JetUncertaintiesTool) );
    ANA_CHECK( m_JetUncertaintiesTool_handle.setProperty("JetDefinition", "AntiKt4EMPFlow") );
    ANA_CHECK( m_JetUncertaintiesTool_handle.setProperty("MCType", "MC16") );
    ANA_CHECK( m_JetUncertaintiesTool_handle.setProperty("IsData", m_isData) );
    ANA_CHECK( m_JetUncertaintiesTool_handle.setProperty("ConfigFile", "rel21/Fall2018/R4_GlobalReduction_SimpleJER.config") );
    ANA_CHECK( m_JetUncertaintiesTool_handle.setProperty("Path", "$WorkDir_DIR/data/"));
    ANA_CHECK( m_JetUncertaintiesTool_handle.setProperty("AnalysisFile", "fractaljets/DijetFlavourComp_Run2.root") );
    ANA_CHECK( m_JetUncertaintiesTool_handle.retrieve() );
    ANA_MSG_DEBUG("Retrieved tool: " << m_JetUncertaintiesTool_handle);
  */
  ANA_CHECK( ASG_MAKE_ANA_TOOL(m_JetUncertaintiesTool_handle, JetUncertaintiesTool) );
  ANA_CHECK( m_JetUncertaintiesTool_handle.setProperty("JetDefinition", "AntiKt10LCTopoTrimmedPtFrac5SmallR20") );
  ANA_CHECK( m_JetUncertaintiesTool_handle.setProperty("MCType", "MC16") );
  ANA_CHECK( m_JetUncertaintiesTool_handle.setProperty("IsData", m_isData) );
  ANA_CHECK( m_JetUncertaintiesTool_handle.setProperty("ConfigFile", "rel21/Summer2019/R10_GlobalReduction.config") );
  ANA_CHECK( m_JetUncertaintiesTool_handle.setProperty("Path", "$WorkDir_DIR/data/"));
  //ANA_CHECK( m_JetUncertaintiesTool_handle.setProperty("AnalysisFile", "fractaljets/DijetFlavourComp_Run2.root") );
  ANA_CHECK( m_JetUncertaintiesTool_handle.retrieve() );
  ANA_MSG_DEBUG("Retrieved tool: " << m_JetUncertaintiesTool_handle);
  
  if(!m_isData)
    {
      CP::SystematicSet recommendedSystematics;
      recommendedSystematics.insert (m_JetUncertaintiesTool_handle->recommendedSystematics());
      m_systematicsList = CP::make_systematics_vector(recommendedSystematics);      
      
      // Generate nominal systematic
      m_systematicsList.insert(m_systematicsList.begin(), CP::SystematicSet());
      
      // PDF weights
      nnpdf23_set = new LHAPDF::PDFSet("NNPDF23_lo_as_0119_qed");
      pdfs = nnpdf23_set->mkPDFs(); // pointers to PDF set members
      myrand_global = new TRandom3(0); //for uncerts.
    }
  else 
    {
      // for data, we only need the nominal systematic
      m_systematicsList.insert(m_systematicsList.begin(), CP::SystematicSet());
    }
  
  /*
    // here be the jes nps yo 
    
    nominal
    JetCalibrator.SmallJet...INFO     JET_BJES_Response__1up
    JetCalibrator.SmallJet...INFO     JET_BJES_Response__1down
    JetCalibrator.SmallJet...INFO     JET_EffectiveNP_1__1up
    JetCalibrator.SmallJet...INFO     JET_EffectiveNP_1__1down
    JetCalibrator.SmallJet...INFO     JET_EffectiveNP_2__1up
    JetCalibrator.SmallJet...INFO     JET_EffectiveNP_2__1down
    JetCalibrator.SmallJet...INFO     JET_EffectiveNP_3__1up
    JetCalibrator.SmallJet...INFO     JET_EffectiveNP_3__1down
    JetCalibrator.SmallJet...INFO     JET_EffectiveNP_4__1up
    JetCalibrator.SmallJet...INFO     JET_EffectiveNP_4__1down
    JetCalibrator.SmallJet...INFO     JET_EffectiveNP_5__1up
    JetCalibrator.SmallJet...INFO     JET_EffectiveNP_5__1down
    JetCalibrator.SmallJet...INFO     JET_EffectiveNP_6__1up
    JetCalibrator.SmallJet...INFO     JET_EffectiveNP_6__1down
    JetCalibrator.SmallJet...INFO     JET_EffectiveNP_7__1up
    JetCalibrator.SmallJet...INFO     JET_EffectiveNP_7__1down
    JetCalibrator.SmallJet...INFO     JET_EffectiveNP_8restTerm__1up
    JetCalibrator.SmallJet...INFO     JET_EffectiveNP_8restTerm__1down
    JetCalibrator.SmallJet...INFO     JET_EtaIntercalibration_Modelling__1up
    JetCalibrator.SmallJet...INFO     JET_EtaIntercalibration_Modelling__1down
    JetCalibrator.SmallJet...INFO     JET_EtaIntercalibration_NonClosure_highE__1up
    JetCalibrator.SmallJet...INFO     JET_EtaIntercalibration_NonClosure_highE__1down
    JetCalibrator.SmallJet...INFO     JET_EtaIntercalibration_NonClosure_negEta__1up
    JetCalibrator.SmallJet...INFO     JET_EtaIntercalibration_NonClosure_negEta__1down
    JetCalibrator.SmallJet...INFO     JET_EtaIntercalibration_NonClosure_posEta__1up
    JetCalibrator.SmallJet...INFO     JET_EtaIntercalibration_NonClosure_posEta__1down
    JetCalibrator.SmallJet...INFO     JET_EtaIntercalibration_TotalStat__1up
    JetCalibrator.SmallJet...INFO     JET_EtaIntercalibration_TotalStat__1down
    JetCalibrator.SmallJet...INFO     JET_Flavor_Composition__1up
    JetCalibrator.SmallJet...INFO     JET_Flavor_Composition__1down
    JetCalibrator.SmallJet...INFO     JET_Flavor_Response__1up
    JetCalibrator.SmallJet...INFO     JET_Flavor_Response__1down
    JetCalibrator.SmallJet...INFO     JET_JER_DataVsMC_MC16__1up
    JetCalibrator.SmallJet...INFO     JET_JER_DataVsMC_MC16__1down
    JetCalibrator.SmallJet...INFO     JET_JER_EffectiveNP_1__1up
    JetCalibrator.SmallJet...INFO     JET_JER_EffectiveNP_1__1down
    JetCalibrator.SmallJet...INFO     JET_JER_EffectiveNP_2__1up
    JetCalibrator.SmallJet...INFO     JET_JER_EffectiveNP_2__1down
    JetCalibrator.SmallJet...INFO     JET_JER_EffectiveNP_3__1up
    JetCalibrator.SmallJet...INFO     JET_JER_EffectiveNP_3__1down
    JetCalibrator.SmallJet...INFO     JET_JER_EffectiveNP_4__1up
    JetCalibrator.SmallJet...INFO     JET_JER_EffectiveNP_4__1down
    JetCalibrator.SmallJet...INFO     JET_JER_EffectiveNP_5__1up
    JetCalibrator.SmallJet...INFO     JET_JER_EffectiveNP_5__1down
    JetCalibrator.SmallJet...INFO     JET_JER_EffectiveNP_6__1up
    JetCalibrator.SmallJet...INFO     JET_JER_EffectiveNP_6__1down
    JetCalibrator.SmallJet...INFO     JET_JER_EffectiveNP_7restTerm__1up
    JetCalibrator.SmallJet...INFO     JET_JER_EffectiveNP_7restTerm__1down
    JetCalibrator.SmallJet...INFO     JET_Pileup_OffsetMu__1up
    JetCalibrator.SmallJet...INFO     JET_Pileup_OffsetMu__1down
    JetCalibrator.SmallJet...INFO     JET_Pileup_OffsetNPV__1up
    JetCalibrator.SmallJet...INFO     JET_Pileup_OffsetNPV__1down
    JetCalibrator.SmallJet...INFO     JET_Pileup_PtTerm__1up
    JetCalibrator.SmallJet...INFO     JET_Pileup_PtTerm__1down
    JetCalibrator.SmallJet...INFO     JET_Pileup_RhoTopology__1up
    JetCalibrator.SmallJet...INFO     JET_Pileup_RhoTopology__1down
    JetCalibrator.SmallJet...INFO     JET_PunchThrough_MC16__1up
    JetCalibrator.SmallJet...INFO     JET_PunchThrough_MC16__1down
    JetCalibrator.SmallJet...INFO     JET_SingleParticle_HighPt__1up
    JetCalibrator.SmallJet...INFO     JET_SingleParticle_HighPt__1down
  */
  
  t->Branch("DSID",&m_DSID);
  t->Branch("mcWeight",&m_mcWeight);
  m_showerWeights = new std::vector<float>();
  t->Branch("showerWeights",&m_showerWeights);
  m_pdfWeights = new std::vector<float>();
  t->Branch("pdfWeights",&m_pdfWeights);
  t->Branch("weight_pileup", &m_weight_pileup);
  t->Branch("weight_pileup_up", &m_weight_pileup_up);
  t->Branch("weight_pileup_down", &m_weight_pileup_down);
  t->Branch("mu",&m_mu);
  t->Branch("npv",&m_npv);
  t->Branch("RunNumber",&m_RunNumber);

  t->Branch("passL1",&m_passL1);
  t->Branch("passHLT",&m_passHLT);

  t->Branch("passedTriggers",&m_passedTriggers);
  t->Branch("disabledTriggers",&m_disabledTriggers);  
  t->Branch("triggerPrescales",&m_triggerPrescales);
  t->Branch("isPassBitsNames",&m_isPassBitsNames);
  t->Branch("isPassBits",&m_isPassBits);

  t->Branch("tjet_N",&m_tjet_N);
  //m_tjet1_Pt = new std::vector<float>();
  t->Branch("tjet1_Pt",&m_tjet1_Pt);
  //m_tjet1_Eta = new std::vector<float>();
  t->Branch("tjet1_Eta",&m_tjet1_Eta);
  t->Branch("tjet1_y",&m_tjet1_y);
  //m_tjet1_Phi = new std::vector<float>();
  t->Branch("tjet1_Phi",&m_tjet1_Phi);
  //m_tjet1_E = new std::vector<float>();
  t->Branch("tjet1_E",&m_tjet1_E);
  m_tjet1_trk_pt = new std::vector<float>();
  t->Branch("tjet1_trk_pt",&m_tjet1_trk_pt);
  m_tjet1_trk_eta = new std::vector<float>();
  t->Branch("tjet1_trk_eta",&m_tjet1_trk_eta);
  m_tjet1_trk_y = new std::vector<float>();
  t->Branch("tjet1_trk_y",&m_tjet1_trk_y);
  m_tjet1_trk_phi = new std::vector<float>();
  t->Branch("tjet1_trk_phi",&m_tjet1_trk_phi);
  m_tjet1_trk_E = new std::vector<float>();
  t->Branch("tjet1_trk_E",&m_tjet1_trk_E);  
  m_tjet1_trk_q = new std::vector<int>();
  t->Branch("tjet1_trk_q",&m_tjet1_trk_q);
  m_tjet1_trk_index = new std::vector<int>(); // We don't need this to be written out

  //m_tjet2_Pt = new std::vector<float>();
  t->Branch("tjet2_Pt",&m_tjet2_Pt);
  //m_tjet2_Eta = new std::vector<float>();
  t->Branch("tjet2_Eta",&m_tjet2_Eta);
  t->Branch("tjet2_y",&m_tjet2_y);
  //m_tjet2_Phi = new std::vector<float>();
  t->Branch("tjet2_Phi",&m_tjet2_Phi);
  //m_tjet2_E = new std::vector<float>();
  t->Branch("tjet2_E",&m_tjet2_E);
  m_tjet2_trk_pt = new std::vector<float>();
  t->Branch("tjet2_trk_pt",&m_tjet2_trk_pt);
  m_tjet2_trk_eta = new std::vector<float>();
  t->Branch("tjet2_trk_eta",&m_tjet2_trk_eta);
  m_tjet2_trk_y = new std::vector<float>();
  t->Branch("tjet2_trk_y",&m_tjet2_trk_y);
  m_tjet2_trk_phi = new std::vector<float>();
  t->Branch("tjet2_trk_phi",&m_tjet2_trk_phi);
  m_tjet2_trk_E = new std::vector<float>();
  t->Branch("tjet2_trk_E",&m_tjet2_trk_E);
  m_tjet2_trk_q = new std::vector<int>();
  t->Branch("tjet2_trk_q",&m_tjet2_trk_q);
  m_tjet2_trk_index = new std::vector<int>(); // We don't need this to be written out

  m_rjet_N = new std::vector<int>();
  t->Branch("rjet_N",&m_rjet_N);
  m_rjet1_Pt = new std::vector<float>();
  t->Branch("rjet1_Pt",&m_rjet1_Pt);
  m_rjet1_Eta = new std::vector<float>();
  t->Branch("rjet1_Eta",&m_rjet1_Eta);
  m_rjet1_y = new std::vector<float>();
  t->Branch("rjet1_y",&m_rjet1_y);
  m_rjet1_Phi = new std::vector<float>();
  t->Branch("rjet1_Phi",&m_rjet1_Phi);
  m_rjet1_E = new std::vector<float>();
  t->Branch("rjet1_E",&m_rjet1_E);
  m_rjet1_Label = new std::vector<int>();
  t->Branch("rjet1_Label",&m_rjet1_Label);
  m_rjet1_NTrk = new std::vector<int>();
  t->Branch("rjet1_NTrk",&m_rjet1_NTrk);
  m_rjet1_trk_pt = new std::vector<std::vector<float>>();
  t->Branch("rjet1_trk_pt",&m_rjet1_trk_pt);
  m_rjet1_trk_eta = new std::vector<std::vector<float>>();
  t->Branch("rjet1_trk_eta",&m_rjet1_trk_eta);
  m_rjet1_trk_y = new std::vector<std::vector<float>>();
  t->Branch("rjet1_trk_y",&m_rjet1_trk_y);
  m_rjet1_trk_phi = new std::vector<std::vector<float>>();
  t->Branch("rjet1_trk_phi",&m_rjet1_trk_phi);
  m_rjet1_trk_E = new std::vector<std::vector<float>>();
  t->Branch("rjet1_trk_E",&m_rjet1_trk_E);  
  m_rjet1_trk_truthIndex1 = new std::vector<std::vector<int>>();
  t->Branch("rjet1_trk_truthIndex1",&m_rjet1_trk_truthIndex1);  
  m_rjet1_trk_truthIndex2 = new std::vector<std::vector<int>>();
  t->Branch("rjet1_trk_truthIndex2",&m_rjet1_trk_truthIndex2);  


  m_rjet2_Pt = new std::vector<float>();
  t->Branch("rjet2_Pt",&m_rjet2_Pt);
  m_rjet2_Eta = new std::vector<float>();
  t->Branch("rjet2_Eta",&m_rjet2_Eta);
  m_rjet2_y = new std::vector<float>();
  t->Branch("rjet2_y",&m_rjet2_y);
  m_rjet2_Phi = new std::vector<float>();
  t->Branch("rjet2_Phi",&m_rjet2_Phi);
  m_rjet2_E = new std::vector<float>();
  t->Branch("rjet2_E",&m_rjet2_E);
  m_rjet2_Label = new std::vector<int>();
  t->Branch("rjet2_Label",&m_rjet2_Label);
  m_rjet2_NTrk = new std::vector<int>();
  t->Branch("rjet2_NTrk",&m_rjet2_NTrk);
  m_rjet2_trk_pt = new std::vector<std::vector<float>>();
  t->Branch("rjet2_trk_pt",&m_rjet2_trk_pt);
  m_rjet2_trk_eta = new std::vector<std::vector<float>>();
  t->Branch("rjet2_trk_eta",&m_rjet2_trk_eta);
  m_rjet2_trk_y = new std::vector<std::vector<float>>();
  t->Branch("rjet2_trk_y",&m_rjet2_trk_y);
  m_rjet2_trk_phi = new std::vector<std::vector<float>>();
  t->Branch("rjet2_trk_phi",&m_rjet2_trk_phi);
  m_rjet2_trk_E = new std::vector<std::vector<float>>();
  t->Branch("rjet2_trk_E",&m_rjet2_trk_E);
  m_rjet2_trk_truthIndex1 = new std::vector<std::vector<int>>();
  t->Branch("rjet2_trk_truthIndex1",&m_rjet2_trk_truthIndex1);
  m_rjet2_trk_truthIndex2 = new std::vector<std::vector<int>>();
  t->Branch("rjet2_trk_truthIndex2",&m_rjet2_trk_truthIndex2);

  return StatusCode::SUCCESS;
}

StatusCode TrackJetTreeAlgo :: execute ()
{
  static int count = 0;
  bool debug=false;
  if(count==0)
    {
      debug = true;
      std::cout << "TrackJetTreeAlgo :: execute() BEGIN" << std::endl;
    }
  count++;
  if(count % m_MessageFrequency == 0) debug = true; // Defaults to print every 1000 events

  if(debug) std::cout << "TrackJetTreeAlgo :: execute()\tProcessing event " << count << ". "  << std::endl;

  int npv = 0;
  const xAOD::VertexContainer* PrimaryVertices = 0;
  m_event->retrieve(PrimaryVertices,"PrimaryVertices");
  for(const auto vertex : *PrimaryVertices)
    if(vertex->trackParticleLinks().size() >= 2)
      npv++;

  const xAOD::TruthParticleContainer* TruthParticles = 0;
  if(!m_isData) m_event->retrieve(TruthParticles, "TruthParticles");

  const xAOD::JetContainer* Jets=0;
  ANA_CHECK(m_event->retrieve(Jets, m_InputJetCollection.Data()));

  //const xAOD::TrackParticleContainer* InDetTrackParticles = 0;
  //m_event->retrieve(InDetTrackParticles, m_InputTrackCollection.Data());

  const xAOD::EventInfo* eventInfo = 0;
  if( !m_event->retrieve( eventInfo, "EventInfo" ).isSuccess()) return EL::StatusCode::FAILURE;
  
  m_RunNumber = -1;
  m_RunNumber = eventInfo->runNumber();

  if(debug) std::cout << "npv\t" << npv << "\trunnumber\t" << m_RunNumber << std::endl;
  
  m_DSID = -1;  
  Float_t weight = 1.0;

  m_weight_pileup =1.0;
  m_weight_pileup_up = 1.0;
  m_weight_pileup_down =1.0;

  if(!m_isData)
    {      
      Float_t mc_weight=0;
      std::vector<float> shower_weights;
      if(m_doShowerWeights) 
	{ 
	  shower_weights = eventInfo->mcEventWeights(); 
	  mc_weight = shower_weights.at(0);
	}
      else 
	{ 
	  mc_weight = eventInfo->mcEventWeight(); 
	}
      
      if(debug && m_doShowerWeights) 
	std::cout << "MC Weight:\t" << mc_weight << "\tShower Weight 0:\t" << shower_weights.at(0) << std::endl;
      else if(debug && !m_doShowerWeights) std::cout << "MC Weight:\t" << mc_weight << std::endl;

      Int_t dsid = eventInfo->mcChannelNumber();
      
      weight *= mc_weight;
            
      m_DSID=dsid;
      m_showerWeights->clear();
      if(m_doShowerWeights) 
	{
	  for(unsigned int iWeight=0; iWeight<shower_weights.size(); iWeight++)
	    m_showerWeights->push_back(shower_weights.at(iWeight));
	}
      else m_mcWeight=mc_weight;

      ////////////////////////////////////////////////////////////////////////////////
      // Making the PDF weights                                                     //
      ////////////////////////////////////////////////////////////////////////////////
      const xAOD::TruthEventContainer * truthEvents(nullptr);
      m_event->retrieve(truthEvents, "TruthEvents");
      m_pdfWeights->clear();
      for( const auto *truthEvent : *truthEvents)
	{
	  xAOD::TruthEvent::PdfInfo pdf = truthEvent->pdfInfo();
	  for (size_t iPDF = 0; iPDF < nnpdf23_set->size(); iPDF++)
	    {
	      double pdf_weight =  LHAPDF::weightxxQ2( pdf.pdgId1, pdf.pdgId2, pdf.x1, pdf.x2, pdf.Q, pdfs.at(0), pdfs.at(iPDF) );
	      m_pdfWeights->push_back(pdf_weight); // one event weight for each error in the set including the nominal
	    }
	}

      // Pileup weights
      static SG::AuxElement::ConstAccessor< float > weight_pileup ("PileupWeight");
      static SG::AuxElement::ConstAccessor< float > weight_pileup_up ("PileupWeight_UP");
      static SG::AuxElement::ConstAccessor< float > weight_pileup_down ("PileupWeight_DOWN");

      if ( weight_pileup.isAvailable( *eventInfo ) ) { m_weight_pileup = weight_pileup( *eventInfo ); } 
      else { m_weight_pileup = 1.0; }

      if ( weight_pileup_up.isAvailable( *eventInfo ) )  { m_weight_pileup_up = weight_pileup_up( *eventInfo );} 
      else { m_weight_pileup_up = 1.0; }
      
      if ( weight_pileup_down.isAvailable( *eventInfo ) ){ m_weight_pileup_down = weight_pileup_down( *eventInfo );} 
      else { m_weight_pileup_down = 1.0; }
           
      weight *= m_weight_pileup;
    }
  
  m_npv=npv;
  m_mu=eventInfo->averageInteractionsPerCrossing(); // placeholder

  // trigger
  
  m_passL1 = -1;
  m_passHLT = -1;
  m_passedTriggers.clear();
  m_disabledTriggers.clear();
  m_triggerPrescales.clear();
  m_isPassBits.clear();
  m_isPassBitsNames.clear();

  if(m_isData)
    {     
      //static SG::AuxElement::ConstAccessor< int > passAny("passAny");
      static SG::AuxElement::ConstAccessor< int > passL1("passL1");
      if( passL1.isAvailable( *eventInfo ) ) { m_passL1 = passL1( *eventInfo ); }
      else { m_passL1 = -1; }
      static SG::AuxElement::ConstAccessor< int > passHLT("passHLT");
      if( passHLT.isAvailable( *eventInfo ) ) { m_passHLT = passHLT( *eventInfo ); }
      else { m_passHLT = -1; }

      static SG::AuxElement::ConstAccessor< std::vector< std::string > > acc_passedTriggers  ("passedTriggers");
      if( acc_passedTriggers  .isAvailable( *eventInfo ) ) { m_passedTriggers   = acc_passedTriggers  ( *eventInfo ); }
      static SG::AuxElement::ConstAccessor< std::vector< std::string > > acc_disabledTriggers("disabledTriggers");
      if( acc_disabledTriggers.isAvailable( *eventInfo ) ) { m_disabledTriggers = acc_disabledTriggers( *eventInfo ); }

      static SG::AuxElement::ConstAccessor< std::vector< float > > trigPrescales("triggerPrescales");
      if( trigPrescales.isAvailable( *eventInfo ) ) { m_triggerPrescales = trigPrescales( *eventInfo ); }


      static SG::AuxElement::ConstAccessor< std::vector< unsigned int > > isPassBits("isPassedBits");
      if( isPassBits.isAvailable( *eventInfo ) ) { m_isPassBits = isPassBits( *eventInfo ); }
      static SG::AuxElement::ConstAccessor< std::vector< std::string > > isPassBitsNames("isPassedBitsNames");
      if( isPassBitsNames.isAvailable( *eventInfo ) ) { m_isPassBitsNames = isPassBitsNames( *eventInfo ); }
    }

  /////////////////////////////////////////
  // Get truth jets
  //

  m_tjet1_trk_pt->clear();
  m_tjet1_trk_eta->clear();
  m_tjet1_trk_y->clear();
  m_tjet1_trk_phi->clear();
  m_tjet1_trk_E->clear();
  m_tjet1_trk_q->clear();
  m_tjet1_trk_index->clear();

  m_tjet2_trk_pt->clear();
  m_tjet2_trk_eta->clear();
  m_tjet2_trk_y->clear();
  m_tjet2_trk_phi->clear();
  m_tjet2_trk_E->clear();
  m_tjet2_trk_q->clear();
  m_tjet2_trk_index->clear();

  double R = m_jetRadius;
  double ptmin = m_jetMinPt/1.5;
  fastjet::JetDefinition jet_def(fastjet::antikt_algorithm, R);

  int iTruth=0;
  std::vector<fastjet::PseudoJet> v_pj_akt10truth;
  if(!m_isData)
    {
      std::vector<fastjet::PseudoJet> v_pj_TruthParticles;
      for (const auto *c : *TruthParticles)
        {
          TLorentzVector tlv;
          tlv.SetPtEtaPhiE(c->pt()/1.e3, c->eta(), c->phi(), c->e()/1.e3);
          // minimum 0.5 GeV track pt requirement
          if(tlv.Pt() < 0.5) continue;
          // no muons or neutrinos in our truth jets
          if( c->status() != 1 ) continue;
          if( (fabs(c->pdgId())==13) || (fabs(c->pdgId())==12) || (fabs(c->pdgId())==14) || (fabs(c->pdgId())==16) ) continue;

      	  fastjet::PseudoJet pj(tlv.Px(),
                                tlv.Py(),
                                tlv.Pz(),
                                tlv.E());

          v_pj_TruthParticles.push_back(pj);
        }
      
      fastjet::ClusterSequence clust_seq_akt10truth(v_pj_TruthParticles, jet_def);
      v_pj_akt10truth = sorted_by_pt(clust_seq_akt10truth.inclusive_jets(ptmin));
    }
  
  for(unsigned int iJet=0; iJet<v_pj_akt10truth.size(); iJet++)
    {
      if(iJet>1) continue;

      if(debug) std::cout << "Processing truth jet\t" << iJet << std::endl;
      
      TLorentzVector tlv_jet;
      tlv_jet.SetPtEtaPhiE(v_pj_akt10truth.at(iJet).perp(),
			   v_pj_akt10truth.at(iJet).eta(),
			   v_pj_akt10truth.at(iJet).phi(),
			   v_pj_akt10truth.at(iJet).e());
      
      if(iTruth==0)
	{
	  m_tjet1_Pt=tlv_jet.Pt();
	  m_tjet1_Eta=tlv_jet.Eta();
          m_tjet1_y=tlv_jet.Rapidity();
          m_tjet1_Phi=tlv_jet.Phi();
          m_tjet1_E=tlv_jet.E();
	}
      if(iTruth==1)
	{
      	  m_tjet2_Pt=tlv_jet.Pt();
          m_tjet2_Eta=tlv_jet.Eta();
          m_tjet2_y=tlv_jet.Rapidity();
          m_tjet2_Phi=tlv_jet.Phi();
          m_tjet2_E=tlv_jet.E();
	  
	}
     
      std::vector<fastjet::PseudoJet> v_pj_ChargedTruthParticles;
      for (const auto *c : *TruthParticles)
	{
	  if( c->status() != 1 ) continue;
	  
	  // no muons or neutrinos
	  if( (fabs(c->pdgId())==13) || (fabs(c->pdgId())==12) || (fabs(c->pdgId())==14) || (fabs(c->pdgId())==16) ) continue;
	  
	  if( c->charge() == 0 ) continue; // only charged particles
	  
	  bool isMatchedToJet=false;
	  
	  TLorentzVector tlv;
	  tlv.SetPtEtaPhiE(c->pt()/1.e3, c->eta(), c->phi(), c->e()/1.e3);

	  if( tlv.Pt() < 0.5) continue; // We select tracks with at least 0.5 GeV of energy
	  if( tlv.DeltaR(tlv_jet) < m_jetRadius ) isMatchedToJet=true;

	  if(isMatchedToJet)
	    {
	      if(iTruth==0)
		{
		  m_tjet1_trk_pt->push_back(tlv.Pt());
		  m_tjet1_trk_eta->push_back(tlv.Eta());
		  m_tjet1_trk_y->push_back(tlv.Rapidity());
		  m_tjet1_trk_phi->push_back(tlv.Phi());
		  m_tjet1_trk_E->push_back(tlv.E());
		  m_tjet1_trk_q->push_back(c->charge());
		  //Find the index of this truth particle
		  auto it = std::find(TruthParticles->begin(), TruthParticles->end(), c);
		  int index = std::distance(TruthParticles->begin(), it);
		  m_tjet1_trk_index->push_back(index);
		}
	      if(iTruth==1)
		{
		  m_tjet2_trk_pt->push_back(tlv.Pt());
		  m_tjet2_trk_eta->push_back(tlv.Eta());
		  m_tjet2_trk_y->push_back(tlv.Rapidity());
		  m_tjet2_trk_phi->push_back(tlv.Phi());
		  m_tjet2_trk_E->push_back(tlv.E());
		  m_tjet2_trk_q->push_back(c->charge());
		  //Find the index of this truth particle
		  auto it = std::find(TruthParticles->begin(), TruthParticles->end(), c);
		  int index = std::distance(TruthParticles->begin(), it);
		  m_tjet2_trk_index->push_back(index);
		}
	    }
	}
      iTruth++;
    }
  m_tjet_N = iTruth;  

  /////////////////////////////////////////
  // Get reco jets
  // 

  m_rjet1_Pt->clear();
  m_rjet1_Eta->clear();
  m_rjet1_y->clear();
  m_rjet1_Phi->clear();
  m_rjet1_E->clear();
  m_rjet1_Label->clear();
  m_rjet1_NTrk->clear();

  m_rjet2_Pt->clear();
  m_rjet2_Eta->clear();
  m_rjet2_y->clear();
  m_rjet2_Phi->clear();
  m_rjet2_E->clear();
  m_rjet2_Label->clear();
  m_rjet2_NTrk->clear();

  m_rjet1_trk_pt->clear();
  m_rjet1_trk_eta->clear();
  m_rjet1_trk_y->clear();
  m_rjet1_trk_phi->clear();
  m_rjet1_trk_E->clear();
  m_rjet1_trk_truthIndex1->clear();
  m_rjet1_trk_truthIndex2->clear();

  m_rjet2_trk_pt->clear();
  m_rjet2_trk_eta->clear();
  m_rjet2_trk_y->clear();
  m_rjet2_trk_phi->clear();
  m_rjet2_trk_E->clear();
  m_rjet2_trk_truthIndex1->clear();
  m_rjet2_trk_truthIndex2->clear();

  // tracking systematics 
  std::vector<TString> vec_tracking_systs;
  if(!m_isData)
    {
      vec_tracking_systs.push_back("PVTrackParticles_InDetTrackParticles");
      vec_tracking_systs.push_back("PVTrackParticles_TruthFilteredInDetTrackParticles");
      vec_tracking_systs.push_back("PVTrackParticles_TruthFilteredInDetTrackParticles2");
      vec_tracking_systs.push_back("PVTrackParticles_BiasedInDetTrackParticles");
      vec_tracking_systs.push_back("PVTrackParticles_FilteredJetInDetTrackParticles");
    }
  else
    {
      vec_tracking_systs.push_back("PVTrackParticles_BiasedInDetTrackParticles");
    }

  //std::vector<xAOD::TrackParticleContainer*> track_containers;
  //for(unsigned int iTrackingContainer=0; iTrackingContainer<vec_tracking_systs.size(); iTrackingContainer++)

  const xAOD::TrackParticleContainer* nominal_tracks = 0;
  const xAOD::TrackParticleContainer* truthfiltered_tracks = 0;
  const xAOD::TrackParticleContainer* truthfiltered_tracks2 = 0;
  const xAOD::TrackParticleContainer* biased_tracks = 0;
  const xAOD::TrackParticleContainer* filtered_tracks = 0;
  if(!m_isData)
    {
      m_event->retrieve(nominal_tracks, "PVTrackParticles_InDetTrackParticles");
      m_event->retrieve(truthfiltered_tracks, "PVTrackParticles_TruthFilteredInDetTrackParticles");
      m_event->retrieve(truthfiltered_tracks2, "PVTrackParticles_TruthFilteredInDetTrackParticles2");
      m_event->retrieve(biased_tracks, "PVTrackParticles_BiasedInDetTrackParticles");
      m_event->retrieve(filtered_tracks, "PVTrackParticles_FilteredJetInDetTrackParticles");
    }
  else
    {
      m_event->retrieve(biased_tracks, "PVTrackParticles_BiasedInDetTrackParticles");
    }

  int iJet=0;
  for (const auto *jet : *Jets)
    {
      if(iJet>1) continue;

      if(debug) std::cout << "Processing reco jet\t" << iJet << std::endl;

      int iSyst=0;
      for (auto& systematic : m_systematicsList)
	{
	  xAOD::Jet* j = nullptr;
	  m_JetUncertaintiesTool_handle->applySystematicVariation (systematic);
	  m_JetUncertaintiesTool_handle->correctedCopy(*jet, j);
	  
	  // Just making a note here for the future -- are we sure it's ok to take the leading two jets in the selection,
	  // and not just the leading two jets?

	  if(j->pt()/1.e3 < m_jetMinPt) continue;
	  if(fabs(j->eta()) > 2.4) continue;
	  
	  TLorentzVector tlv_jet;
	  tlv_jet.SetPtEtaPhiE(j->pt()/1.e3,
			       j->eta(),
			       j->phi(),
			       j->e()/1.e3);

	  int nTrackContainers = -1;
	  if(m_isData) nTrackContainers = 1;
	  else nTrackContainers = 5;
	  
	  // LOOP OVER TRACK COLLECTIONS HERE 
	  for(unsigned int iTrackingContainer=0; iTrackingContainer<nTrackContainers; iTrackingContainer++)
	    {  
	      if(iSyst!=0 && iTrackingContainer>0) continue;

	      if(m_isData) iTrackingContainer = 3;
	      
	      const xAOD::TrackParticleContainer* tracks = 0;
	      if(iTrackingContainer==0) tracks = nominal_tracks;
	      if(iTrackingContainer==1) tracks = truthfiltered_tracks;
	      if(iTrackingContainer==2) tracks = truthfiltered_tracks2;
	      if(iTrackingContainer==3) tracks = biased_tracks;
	      if(iTrackingContainer==4) tracks = filtered_tracks;
	      
	      std::vector<float> temp_rjet1_trk_pt;
	      std::vector<float> temp_rjet1_trk_eta;
	      std::vector<float> temp_rjet1_trk_y;
	      std::vector<float> temp_rjet1_trk_phi;
	      std::vector<float> temp_rjet1_trk_E;
	      
	      std::vector<float> temp_rjet2_trk_pt;
	      std::vector<float> temp_rjet2_trk_eta;
	      std::vector<float> temp_rjet2_trk_y;
	      std::vector<float> temp_rjet2_trk_phi;
	      std::vector<float> temp_rjet2_trk_E;
	      
	      for (const auto *c : *tracks)
		{	  
		  bool isMatchedToJet=false;
		  
		  TLorentzVector tlv;
		  tlv.SetPtEtaPhiM(c->pt()/1.e3, c->eta(), c->phi(), 0.13957); // charged pion mass
		  if( tlv.E() < 0.5) continue; // We select tracks with at least 0.5 GeV of energy
		  if( tlv.DeltaR(tlv_jet) < m_jetRadius ) isMatchedToJet=true;
		  
		  if(isMatchedToJet)
		    {
		      
		      // Find the matching to a truth particle, if it exists
		      /*
			ElementLink< xAOD::TruthParticleContainer > truthLink;
			int index = -1;
			if (c->isAvailable< ElementLink< xAOD::TruthParticleContainer > > ("truthParticleLink") )
			{
			truthLink = c->auxdata< ElementLink< xAOD::TruthParticleContainer>  >("truthParticleLink");
			// Still need to check if this is in the truth jet!
			if(truthLink.isValid())
			{
			auto it = std::find(TruthParticles->begin(), TruthParticles->end(), (*truthLink));
			index = std::distance(TruthParticles->begin(), it);
			}
			}
		      */
		      
		      if(iJet==0)
			{
			  temp_rjet1_trk_pt.push_back(tlv.Pt());
			  temp_rjet1_trk_eta.push_back(tlv.Eta());
			  temp_rjet1_trk_y.push_back(tlv.Rapidity());
			  temp_rjet1_trk_phi.push_back(tlv.Phi());
			  temp_rjet1_trk_E.push_back(tlv.E());
			}
		      if(iJet==1)
			{
			  temp_rjet2_trk_pt.push_back(tlv.Pt());
			  temp_rjet2_trk_eta.push_back(tlv.Eta());
			  temp_rjet2_trk_y.push_back(tlv.Rapidity());
			  temp_rjet2_trk_phi.push_back(tlv.Phi());
			  temp_rjet2_trk_E.push_back(tlv.E());
			}
		    }
		}
	      
	      if(iJet==0)
		{
		  m_rjet1_NTrk->push_back(temp_rjet1_trk_pt.size());
		  m_rjet1_trk_pt->push_back(temp_rjet1_trk_pt);
		  m_rjet1_trk_eta->push_back(temp_rjet1_trk_eta);
		  m_rjet1_trk_y->push_back(temp_rjet1_trk_y);
		  m_rjet1_trk_phi->push_back(temp_rjet1_trk_phi);
		  m_rjet1_trk_E->push_back(temp_rjet1_trk_E);
		}
	      
	      if(iJet==1)
		{
		  m_rjet2_NTrk->push_back(temp_rjet2_trk_pt.size());
		  m_rjet2_trk_pt->push_back(temp_rjet2_trk_pt);
		  m_rjet2_trk_eta->push_back(temp_rjet2_trk_eta);
		  m_rjet2_trk_y->push_back(temp_rjet2_trk_y);
		  m_rjet2_trk_phi->push_back(temp_rjet2_trk_phi);
		  m_rjet2_trk_E->push_back(temp_rjet2_trk_E);
		}
	    } // END LOOP OVER TRACK COLLECTCIONS
	  
	  if(iJet==0)
	    {
	      //static SG::AuxElement::ConstAccessor<int> PartonTruthLabelID ("PartonTruthLabelID");
	      
	      m_rjet1_Pt->push_back(tlv_jet.Pt());
	      m_rjet1_Eta->push_back(tlv_jet.Eta());
	      m_rjet1_y->push_back(tlv_jet.Rapidity());
	      m_rjet1_Phi->push_back(tlv_jet.Phi());
	      m_rjet1_E->push_back(tlv_jet.E());
	      //m_rjet1_Label->push_back( PartonTruthLabelID(*j) );
	    }
	  if(iJet==1)
	    {
	      //static SG::AuxElement::ConstAccessor<int> PartonTruthLabelID ("PartonTruthLabelID");

	      m_rjet2_Pt->push_back(tlv_jet.Pt());
	      m_rjet2_Eta->push_back(tlv_jet.Eta());
	      m_rjet2_y->push_back(tlv_jet.Rapidity());
	      m_rjet2_Phi->push_back(tlv_jet.Phi());
	      m_rjet2_E->push_back(tlv_jet.E());
	      //m_rjet2_Label->push_back( PartonTruthLabelID(*j) );
	    }

	  iSyst++;
	  delete j;
	} // END LOOP OVER JES/JER

      iJet++;
    } // END LOOP OVER JETS
  
  // Now, use the indices from the truth matching to get an index which can be used directly
  // For now, this is a bit ugly...
  /*
  for(unsigned int rTrack=0; rTrack<(*m_rjet1_trk_truthIndex1).size(); rTrack++)
    {
      int index = (*m_rjet1_trk_truthIndex1)[rTrack];
      if(index < 0) continue;
      int newIndex1 = -1;
      int newIndex2 = -1;
      for(unsigned int tTrack=0; tTrack<(*m_tjet1_trk_index).size(); tTrack++)
        {
          if(index == (*m_tjet1_trk_index)[tTrack]) newIndex1 = tTrack;
        }
      for(unsigned int tTrack=0; tTrack<(*m_tjet2_trk_index).size(); tTrack++)
        {
          if(index == (*m_tjet2_trk_index)[tTrack]) newIndex2 = tTrack;
        }


        (*m_rjet1_trk_truthIndex1)[rTrack] = newIndex1;
        (*m_rjet1_trk_truthIndex2)[rTrack] = newIndex2;
    }
 
  for(unsigned int rTrack=0; rTrack<(*m_rjet2_trk_truthIndex1).size(); rTrack++)
    {
      int index = (*m_rjet2_trk_truthIndex1)[rTrack];
      if(index < 0) continue;
      int newIndex1 = -1;
      int newIndex2 = -1;
      for(unsigned int tTrack=0; tTrack<(*m_tjet1_trk_index).size(); tTrack++)
        {   
          if(index == (*m_tjet1_trk_index)[tTrack]) newIndex1 = tTrack;
        }   
      for(unsigned int tTrack=0; tTrack<(*m_tjet2_trk_index).size(); tTrack++)
        {
          if(index == (*m_tjet2_trk_index)[tTrack]) newIndex2 = tTrack;
        }


        (*m_rjet2_trk_truthIndex1)[rTrack] = newIndex1;
        (*m_rjet2_trk_truthIndex2)[rTrack] = newIndex2;
    }
  */

  if(debug) std::cout << "Filling jets!" << std::endl;
  
  tree (TString("jets_"+m_label).Data())->Fill (); 

  return StatusCode::SUCCESS;
}

StatusCode TrackJetTreeAlgo :: finalize ()
{
  return StatusCode::SUCCESS;
}
