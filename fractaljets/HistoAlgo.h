#ifndef fractaljets_LundHistoAlgo_H
#define fractaljets_LundHistoAlgo_H

#include <iostream>
#include <iomanip>
#include <vector>

//#include <AnaAlgorithm/AnaAlgorithm.h>
#include <EventLoop/Algorithm.h>
#include "EventLoop/OutputStream.h"

#include "TH1F.h"
#include "TH2F.h"
#include "TTree.h"

//#include "xAODRootAccess/TStore.h"
//#include "xAODRootAccess/Init.h"
//#include "xAODRootAccess/TEvent.h"

#include "fastjet/tools/Recluster.hh"
#include "fastjet/contrib/SoftDrop.hh"
#include "fastjet/JetDefinition.hh"

class HistoAlgo : public EL::Algorithm
{
 public:
  HistoAlgo ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(HistoAlgo, 1);

  // Set by config
  Int_t m_MessageFrequency;
  Bool_t m_isData;
  Bool_t m_doJESJER; 
  Int_t m_JESJER_idx;
  Bool_t m_doShowerWeights;
  Int_t m_ShowerWeights_idx;
  Float_t m_jetMinPt;
  Float_t m_jetMaxPt;
  Float_t m_jetMaxEta;
  Float_t m_jetBalance=1.5;
  Float_t m_jetRadius=0.4;
  TString m_label;
  TString m_outDir;
  TString m_PURW;
  Float_t m_LumiShift;

 private:
  // Input variables from TTree
  Int_t m_DSID;
  Int_t m_RunNumber;
  Float_t m_mcWeight;
  //std::vector<float> *m_showerWeights = nullptr;
  Float_t m_weight_pileup;
  Float_t m_weight_pileup_up;
  Float_t m_weight_pileup_down;
  Float_t m_mu;
  Int_t m_npv;
  //std::vector<std::string> *m_PassTrigger = nullptr;

  Int_t m_tjetN;
  float m_tjetPt;
  float m_tjetEta;
  float m_tjetPhi;
  float m_tjetE;

  Int_t m_rjetN;
  
  std::vector<float> *m_rjetPt = nullptr; //!
  std::vector<float> *m_rjetEta = nullptr; //!
  std::vector<float> *m_rjetPhi = nullptr; //!
  std::vector<float> *m_rjetE = nullptr; //!
  
  // output histograms
  TH1F* h1_run = nullptr; //!
  TH1F* h1_npv = nullptr; //!
  TH1F* h1_mu = nullptr; //!

  TH1F* h1_tjetN = nullptr; //!
  TH1F* h1_tjetCount = nullptr; //!
  TH1F* h1_tjetBalance = nullptr; //!
  TH1F* h1_tjetPt = nullptr; //!
  TH1F* h1_tjetEta = nullptr; //!
  TH1F* h1_tjetPhi = nullptr; //!
  TH1F* h1_tjetE = nullptr; //!

};

#endif
