#ifndef fractaljets_TrackJetTreeAlgo_H
#define fractaljets_TrackJetTreeAlgo_H

#include <iostream>
#include <iomanip>
#include <vector>

#include <AnaAlgorithm/AnaAlgorithm.h>
#include "EventLoop/OutputStream.h"

#include "TH1F.h"
#include "TH2F.h"
#include "TTree.h"

#include "xAODRootAccess/TStore.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"

// External Tools
#include "JetCPInterfaces/ICPJetUncertaintiesTool.h"
#include "AsgTools/AnaToolHandle.h"
#include "JetCalibTools/IJetCalibrationTool.h"
#include "JetCalibTools/JetCalibrationTool.h"
#include "JetUncertainties/JetUncertaintiesTool.h"
#include "PATInterfaces/SystematicsUtil.h"

#include "fastjet/tools/Recluster.hh"
#include "fastjet/contrib/SoftDrop.hh"
#include "fastjet/JetDefinition.hh"

#include "LHAPDF/LHAPDF.h"
#include "LHAPDF/Reweighting.h"
#include "TRandom3.h"

class TrackJetTreeAlgo : public EL::AnaAlgorithm
{
 public:
  TrackJetTreeAlgo (const std::string& name, ISvcLocator* pSvcLocator);
  ~TrackJetTreeAlgo();

  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

  xAOD::TEvent *m_event; //!
  
  const LHAPDF::PDFSet *nnpdf23_set; //!
  std::vector<LHAPDF::PDF*> pdfs; //! pointers to PDF set members
  TRandom3* myrand_global; //!for uncerts.

 private:

  // tools
  asg::AnaToolHandle<ICPJetUncertaintiesTool> m_JetUncertaintiesTool_handle {"JetUncertaintiesTool" , this}; //!
  std::vector<CP::SystematicSet> m_systematicsList;

  // Set by config
  Int_t m_MessageFrequency;
  Bool_t m_isData;
  Bool_t m_doJES;
  Bool_t m_doShowerWeights;
  Float_t m_jetMinPt;
  Float_t m_jetMaxPt;
  Float_t m_jetRadius;
  TString m_label;
  TString m_InputTrackCollection;
  TString m_InputJetCollection;
  TString m_PURW;
  
  // Output variables for tree
  Int_t m_DSID;
  Int_t m_RunNumber;
  Float_t m_mcWeight;  
  std::vector<float> *m_showerWeights = nullptr;
  std::vector<float> *m_pdfWeights = nullptr;
  Float_t m_weight_pileup;
  Float_t m_weight_pileup_up;
  Float_t m_weight_pileup_down;  
  Float_t m_mu;
  Int_t m_npv;

  int m_tjet_N;
  float m_tjet1_Pt;// = nullptr;
  float m_tjet1_Eta;// = nullptr;
  float m_tjet1_y;// = nullptr;
  float m_tjet1_Phi;// = nullptr;
  float m_tjet1_E;// = nullptr;    
  float m_tjet1_NTrk;// = nullptr;
  std::vector<float> *m_tjet1_trk_pt = nullptr;
  std::vector<float> *m_tjet1_trk_eta = nullptr;
  std::vector<float> *m_tjet1_trk_y = nullptr;
  std::vector<float> *m_tjet1_trk_phi = nullptr;
  std::vector<float> *m_tjet1_trk_E = nullptr;  
  std::vector<int> *m_tjet1_trk_q = nullptr;
  std::vector<int> *m_tjet1_trk_index = nullptr;  
  float m_tjet2_Pt;// = nullptr;
  float m_tjet2_Eta;// = nullptr;
  float m_tjet2_y;// = nullptr;
  float m_tjet2_Phi;// = nullptr;
  float m_tjet2_E;// = nullptr;
  float m_tjet2_NTrk;// = nullptr;
  std::vector<float> *m_tjet2_trk_pt = nullptr;
  std::vector<float> *m_tjet2_trk_eta = nullptr;
  std::vector<float> *m_tjet2_trk_y = nullptr;
  std::vector<float> *m_tjet2_trk_phi = nullptr;
  std::vector<float> *m_tjet2_trk_E = nullptr; 
  std::vector<int> *m_tjet2_trk_q = nullptr;
  std::vector<int> *m_tjet2_trk_index = nullptr; 
  
  // vector of JES/JER systematics ...
  std::vector<int> *m_rjet_N = nullptr;
  std::vector<float> *m_rjet1_Pt = nullptr;
  std::vector<float> *m_rjet1_Eta = nullptr;
  std::vector<float> *m_rjet1_y = nullptr;
  std::vector<float> *m_rjet1_Phi = nullptr;
  std::vector<float> *m_rjet1_E = nullptr;
  std::vector<int> *m_rjet1_Label = nullptr;
  std::vector<int> *m_rjet1_NTrk = nullptr;
  std::vector<std::vector<int>> *m_rjet1_trk_truthIndex1 = nullptr;
  std::vector<std::vector<int>> *m_rjet1_trk_truthIndex2 = nullptr;
  std::vector<std::vector<float>> *m_rjet1_trk_pt = nullptr;
  std::vector<std::vector<float>> *m_rjet1_trk_eta = nullptr;
  std::vector<std::vector<float>> *m_rjet1_trk_y = nullptr;
  std::vector<std::vector<float>> *m_rjet1_trk_phi = nullptr;
  std::vector<std::vector<float>> *m_rjet1_trk_E = nullptr;

  std::vector<float> *m_rjet2_Pt = nullptr;
  std::vector<float> *m_rjet2_Eta = nullptr;
  std::vector<float> *m_rjet2_y = nullptr;
  std::vector<float> *m_rjet2_Phi = nullptr;
  std::vector<float> *m_rjet2_E = nullptr;
  std::vector<int> *m_rjet2_Label = nullptr;
  std::vector<int> *m_rjet2_NTrk = nullptr;
  std::vector<std::vector<int>> *m_rjet2_trk_truthIndex1 = nullptr;
  std::vector<std::vector<int>> *m_rjet2_trk_truthIndex2 = nullptr;
  std::vector<std::vector<float>> *m_rjet2_trk_pt = nullptr;
  std::vector<std::vector<float>> *m_rjet2_trk_eta = nullptr;
  std::vector<std::vector<float>> *m_rjet2_trk_y = nullptr;
  std::vector<std::vector<float>> *m_rjet2_trk_phi = nullptr;
  std::vector<std::vector<float>> *m_rjet2_trk_E = nullptr;
  
  // trigger
  int m_passL1;
  int m_passHLT;

  // jet trigger
  std::vector<std::string> m_passedTriggers;
  std::vector<std::string> m_disabledTriggers;
  std::vector<float> m_triggerPrescales;
  std::vector<std::string>  m_isPassBitsNames;
  std::vector<unsigned int> m_isPassBits;
  
};

#endif
