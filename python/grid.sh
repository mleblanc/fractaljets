#!/bin/bash

xAH_run.py --config src/fractaljets/python/xAH_Trees.py \
    --files src/fractaljets/data/DAOD_JETM6_Pythia_p3916.txt \
    --inputList \
    --inputRucio \
    -f \
    prun \
    --optGridOutputSampleName "user.mleblanc.%in:name[2]%.%in:name[3]%.FRACTALSv05"
    --optGridNFilesPerJob 5

xAH_run.py --config src/fractaljets/python/xAH_Trees.py \
    --files src/fractaljets/data/DAOD_JETM6_Sherpa_p3916.txt \
    --inputList \
    --inputRucio \
    -f \
    prun \
    --optGridOutputSampleName "user.mleblanc.%in:name[2]%.%in:name[3]%.FRACTALSv05"
    --optGridNFilesPerJob 5


xAH_run.py --config src/fractaljets/python/xAH_Trees.py \
    --files src/fractaljets/data/DAOD_JETM6_Herwig_p3916.txt \
    --inputList \
    --inputRucio \
    -f \
    prun \
    --optGridOutputSampleName "user.mleblanc.%in:name[2]%.%in:name[3]%.FRACTALSv05"
    --optGridNFilesPerJob 5
