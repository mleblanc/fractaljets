'''
Make trees of jets and the tracks inside them.

MLB 2019
'''

from xAODAnaHelpers import Config
c = Config()

c.output("ANALYSIS")

#c.setalg("MessagePrinterAlgo", {"m_sourceWidth": 60})

c.algorithm("BasicEventSelection", {"m_name": "BasicEventSelector",
                                    "m_isMC" : False,
                                    "m_truthLevelOnly": False,
                                    "m_doPUreweighting": True,
                                    "m_doPUreweightingSys" : True,
                                    "m_autoconfigPRW" : True,
                                    "m_periodConfig" : "auto",
                                    "m_vertexContainerName": "PrimaryVertices",
                                    "m_applyPrimaryVertexCut": False,
                                    "m_applyGRLCut": True,
                                    "m_GRLxml": "GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml,GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml,GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml,GoodRunsLists/data18_13TeV/20180906/data18_13TeV.periodAllYear_DetStatus-v102-pro22-03_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml",
                                    "m_lumiCalcFileNames" : "GoodRunsLists/data18_13TeV/20180906/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-001.root,GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root,GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root,GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root",
                                    #"m_prwActualMu2017File" : "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root",
                                    #"m_prwActualMu2018File" : "GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root",                                    
                                    "m_useMetaData": False,
                                    "m_triggerSelection": "HLT_j25|HLT_j60|HLT_j85|HLT_j110|HLT_j175|HLT_j260|HLT_j420_a10_lcw_L1J100|HLT_j360|HLT_j340|HLT_j380|HLT_j400|HLT_j420|HLT_j450|HLT_j420",
                                    "m_applyTriggerCut": True,
                                    "m_applyEventCleaningCut" : False,
                                    "m_applyCoreFlagsCut"     : True,
                                    "m_applyJetCleaningEventFlag" : True,
                                 })

c.algorithm("JetCalibrator", { "m_name"                  : "LargeJetCalibratorLCTopo",
                               "m_inContainerName"       : "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets",
                               "m_outContainerName"      : "AntiKt10LCTopoTrimmedPtFrac5SmallR20JetsCalib",
                               "m_jetAlgo"               : "AntiKt10LCTopoTrimmedPtFrac5SmallR20",
                               "m_outputAlgo"            : "AntiKt10LCTopoTrimmedPtFrac5SmallR20_Calib_Algo",
                               "m_calibConfigFullSim"    : "JES_MC16recommendation_FatJet_Trimmed_JMS_comb_17Oct2018.config" ,
                               "m_calibConfigData"       : "JES_MC16recommendation_FatJet_Trimmed_JMS_comb_3April2019.config",
                               #"m_uncertConfig"          : "rel21/Summer2019/R10_GlobalReduction.config",
                               "m_calibSequence"         : "EtaJES_JMS_Insitu_InsituCombinedMass",
                               #"m_pseudoData"            : False,
                               #"m_overrideAnalysisFile"  : "$WorkDir_DIR/data/fractaljets/DijetFlavourComp_Run2.root",
                               #"m_overrideCalibArea"     : "00-04-82",
                               "m_doCleaning"            : False,
                               #"m_systVal"               : 1,
                              # "m_systName"              : "Nominal",
                               "m_sort"                  : True
                               })

c.algorithm("JetSelector",{"m_name"             : "JetSelectorAlgo",
                           "m_inContainerName"  : "AntiKt10LCTopoTrimmedPtFrac5SmallR20JetsCalib",
                           "m_outContainerName" : "AntiKt10LCTopoTrimmedPtFrac5SmallR20JetsCalibSel",
                           "m_createSelectedContainer" : True,
                           "m_pT_min"           : 300.e3,
                           "m_eta_max"          : 2.0,
                           "m_doJVT"                   :  False,
                           })

# this one does nothing?
#c.algorithm("InDetTrackTruthOriginToolAlgo", {
#        "m_name" : "InDetTrackTruthOriginToolMLB",
#        "m_inputTrackContainer" : "InDetTrackParticles"
#        })

#c.algorithm("InDetTrackTruthFilterToolAlgo", {
#        "m_name" : "InDetTrackTruthFilterToolAlgoMLB", 
#        "m_inputTrackContainer" : "InDetTrackParticles",
#        "m_outputTrackContainer" : "TruthFilteredInDetTrackParticles",
#        "m_systematic" : "inclusiveeffic"
#        })

#c.algorithm("InDetTrackTruthFilterToolAlgo", {
#        "m_name" : "InDetTrackTruthFilterToolAlgo2MLB",
#        "m_inputTrackContainer" : "InDetTrackParticles",
#        "m_outputTrackContainer" : "TruthFilteredInDetTrackParticles2",
#        "m_systematic" : "fake"
#        })

c.algorithm("InDetTrackBiasingToolAlgo", {
        "m_name" : "InDetTrackBiasingToolAlgoMLB", 
        "m_inputTrackContainer" : "InDetTrackParticles",
        "m_outputTrackContainer" : "BiasedInDetTrackParticles"
        })

#c.algorithm("JetTrackFilterToolAlgo", {
#        "m_name" : "JetTrackFilterTool",
#        "m_JetContainerName" : "AntiKt4EMPFlowJetsCalib",
#        "m_outputTrackContainer" : "FilteredJetInDetTrackParticles"
#        })

for tracks in ["BiasedInDetTrackParticles"]:
    
    c.algorithm("InDetTrackSelectionToolAlgo", {
            "m_name": "InDetTrackSelectionToolAlgo_"+tracks,
            "m_inputTrackContainer": tracks,
            "m_outputTrackContainer": "SelectedTrackParticles_"+tracks,
            "m_CutLevel": "Loose",
            "m_minPt": 2.0e3
            })

    c.algorithm("TightTrackVertexAssociationToolAlgo", {
            "m_name": "TightTrackVertexAssociationToolAlgo_"+tracks,
            "m_inputTrackContainer": "SelectedTrackParticles_"+tracks,
            "m_outputTrackContainer": "PVTrackParticles_"+tracks,
            "m_dzSinTheta_cut": 3,
            "m_doPV": True
            })

c.algorithm("TrackJetTreeAlgo",
            {"m_name": "TreeAlgoMLB",
             "doShowerWeights" : False,
             "doJES" : True,
             "isData" : True,
             "label": tracks,
             "MessageFrequency" : 1000,
             "InputJetCollection" : "AntiKt10LCTopoTrimmedPtFrac5SmallR20JetsCalibSel",
             "jetRadius" : 0.4,
             "jetMinPt" : 300.,
             "jetMaxPt" : 99999.
             })
    
