#!/bin/bash

xAH_run.py --config src/fractaljets/python/xAH_Trees.py \
    --files src/fractaljets/data/boosted_ws.txt \
    --inputList \
    --inputRucio \
    -f \
    prun \
    --optGridOutputSampleName "user.mleblanc.%in:name[2]%.%in:name[3]%.WPRIMEv01"    

xAH_run.py --config src/fractaljets/python/xAH_Trees.py \
    --files src/fractaljets/data/boosted_tops.txt \
    --inputList \
    --inputRucio \
    -f \
    prun \
    --optGridOutputSampleName "user.mleblanc.%in:name[2]%.%in:name[3]%.ZPRIMEv01"
