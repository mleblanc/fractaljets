import math
import re
import ROOT
import numpy as np
import h5py
#import JetPlots

# energyflow imports
import energyflow as ef
from energyflow.archs import PFN
from energyflow.datasets import qg_jets
from energyflow.utils import data_split, remap_pids, to_categorical

# attempt to import sklearn
try:
    from sklearn.metrics import roc_auc_score, roc_curve
except:
    print('please install scikit-learn in order to make ROC curves')
    roc_curve = False

# attempt to import matplotlib
try:
    import matplotlib.pyplot as plt
except:
    print('please install matploltib in order to make plots')
    plt = False

#in_file_sherpa_cluster = "in/mc16_13TeV:mc16_13TeV.364683.Sherpa_CT10_CT14nnlo_CSShower_2to2jets_JZ7.deriv.DAOD_JETM6.e6929_e5984_s3126_r10201_r10210_p3916.root"
#in_file_sherpa_lund    = "in/mc16_13TeV:mc16_13TeV.364692.Sherpa_CT10_CT14nnlo_CSShower_Lund_2to2jets_JZ7.deriv.DAOD_JETM6.e6929_e5984_s3126_r10201_r10210_p3916.root"

in_file_sherpa_cluster = "in/mc16_13TeV.364683.Sherpa_CT10_CT14nnlo_CSShower_2to2jets_JZ7.deriv.DAOD_JETM6.e6929_e5984_s3126_r10201_r10210_p3916.root"
in_file_sherpa_lund    = "in/mc16_13TeV.364692.Sherpa_CT10_CT14nnlo_CSShower_Lund_2to2jets_JZ7.deriv.DAOD_JETM6.e6929_e5984_s3126_r10201_r10210_p3916.root"

file_sherpa_cluster = ROOT.TFile.Open(in_file_sherpa_cluster)
t_sherpa_cluster = file_sherpa_cluster.Get("jets_FilteredJetInDetTrackParticles")

file_sherpa_lund = ROOT.TFile.Open(in_file_sherpa_lund)
t_sherpa_lund = file_sherpa_lund.Get("jets_FilteredJetInDetTrackParticles")

# print tree contents
t_sherpa_cluster.Print()

# Cluster stuff
h1_sherpa_cluster_tjet1_Pt = ROOT.TH1D("h1_sherpa_cluster_tjet1_Pt","h1_sherpa_cluster_tjet1_Pt",300,0,3000)
h1_sherpa_cluster_tjet2_Pt = ROOT.TH1D("h1_sherpa_cluster_tjet2_Pt","h1_sherpa_cluster_tjet2_Pt",300,0,3000)
h1_sherpa_cluster_rjet1_Pt = ROOT.TH1D("h1_sherpa_cluster_rjet1_Pt","h1_sherpa_cluster_rjet1_Pt",300,0,3000)
h1_sherpa_cluster_rjet2_Pt = ROOT.TH1D("h1_sherpa_cluster_rjet2_Pt","h1_sherpa_cluster_rjet2_Pt",300,0,3000)
#h2_track_occupancy_cluster = ROOT.TH2F("h2_track_occupancy_cluster","h2_track_occupancy_cluster",50,-2.5,2.5,64,-3.2,3.2)
h2_track_occupancy_cluster = ROOT.TH2F("h2_track_occupancy_cluster","h2_track_occupancy_cluster",25,-0.5,0.5,25,-0.5,0.5)

cluster_jet_array = np.zeros( (t_sherpa_cluster.GetEntries()*2, 100, 3 ) )
for event in range(0, t_sherpa_cluster.GetEntries()):
    t_sherpa_cluster.GetEntry(event)
    if(event%1000==0): print('event %s / %s SHERPA CLUSTER',event,t_sherpa_cluster.GetEntries())
    h1_sherpa_cluster_tjet1_Pt.Fill(t_sherpa_cluster.tjet1_Pt,t_sherpa_cluster.mcWeight)
    h1_sherpa_cluster_tjet2_Pt.Fill(t_sherpa_cluster.tjet2_Pt,t_sherpa_cluster.mcWeight)
    
    # leading jet
    jet_track_array_1 = np.zeros( (100, 3) )
    if(len(t_sherpa_cluster.rjet1_Pt)>0):
        h1_sherpa_cluster_rjet1_Pt.Fill(t_sherpa_cluster.rjet1_Pt[0],t_sherpa_cluster.mcWeight)
 
        for track in range(0,100): # gotta zero pad
            if(track < len(t_sherpa_cluster.rjet1_trk_pt[0])):
                pt = t_sherpa_cluster.rjet1_trk_pt[0][track]
                eta = t_sherpa_cluster.rjet1_trk_eta[0][track]
                phi = t_sherpa_cluster.rjet1_trk_phi[0][track]
                E = t_sherpa_cluster.rjet1_trk_E[0][track]
                jet_track_array_1[track,0] = pt*1.e3
                jet_track_array_1[track,1] = eta-t_sherpa_cluster.rjet1_Eta[0]
                jet_track_array_1[track,2] = phi-t_sherpa_cluster.rjet1_Phi[0]
                #jet_track_array_1[track,3] = E*1.e3                
                h2_track_occupancy_cluster.Fill(eta-t_sherpa_cluster.rjet1_Eta[0],
                                                phi-t_sherpa_cluster.rjet1_Phi[0],
                                                pt*t_sherpa_cluster.mcWeight)
            else:
                jet_track_array_1[track,0] = 0
                jet_track_array_1[track,1] = 0
                jet_track_array_1[track,2] = 0
                #jet_track_array_1[track,3] = 0
        if(len(t_sherpa_cluster.rjet1_trk_pt[0])>0): jet_track_array_1[:,0]/=np.sum(jet_track_array_1[:,0],0)
        cluster_jet_array[event] = jet_track_array_1

    # subleading jet
    '''
    jet_track_array_2 = np.zeros( (100, 3) )
    if(len(t_sherpa_cluster.rjet2_Pt)>0):
        h1_sherpa_cluster_rjet2_Pt.Fill(t_sherpa_cluster.rjet2_Pt[0],t_sherpa_cluster.mcWeight)  

        avg_eta=0.
        avg_phi=0.
        for track in range(0,len(t_sherpa_cluster.rjet2_trk_pt[0])):
            avg_eta += t_sherpa_cluster.rjet2_trk_eta[0][track]
            avg_phi += t_sherpa_cluster.rjet2_trk_phi[0][track]
        avg_eta/len(t_sherpa_cluster.rjet2_trk_pt[0])
        avg_phi/len(t_sherpa_cluster.rjet2_trk_pt[0])

        # for track in range(0,len(t_sherpa_cluster.rjet1_trk_pt[0])):
        for track in range(0,100): # gotta zero pad
            if(track < len(t_sherpa_cluster.rjet2_trk_pt[0])):
                pt = t_sherpa_cluster.rjet2_trk_pt[0][track]
                eta = t_sherpa_cluster.rjet2_trk_eta[0][track]
                phi = t_sherpa_cluster.rjet2_trk_phi[0][track]
                E = t_sherpa_cluster.rjet2_trk_E[0][track]
                jet_track_array_2[track,0] = pt*1.e3
                jet_track_array_2[track,1] = eta
                jet_track_array_2[track,2] = phi
                jet_track_array_2[track,3] = E*1.e3
                h2_track_occupancy_cluster.Fill(eta,phi,pt*t_sherpa_cluster.mcWeight)
            else:
                jet_track_array_2[track,0] = 0
                jet_track_array_2[track,1] = 0
                jet_track_array_2[track,2] = 0
                jet_track_array_2[track,3] = 0
    cluster_jet_array[event+t_sherpa_cluster.GetEntries()] = jet_track_array_2
    '''

print(cluster_jet_array)
print(cluster_jet_array.shape)

h5f_cluster = h5py.File('cluster_jz7.h5', 'w')
h5f_cluster.create_dataset('cluster_sherpa_jz7', data=cluster_jet_array)
h5f_cluster.close()

# Lund stuff
h1_sherpa_lund_tjet1_Pt = ROOT.TH1D("h1_sherpa_lund_tjet1_Pt","h1_sherpa_lund_tjet1_Pt",300,0,3000)
h1_sherpa_lund_tjet2_Pt = ROOT.TH1D("h1_sherpa_lund_tjet2_Pt","h1_sherpa_lund_tjet2_Pt",300,0,3000)
h1_sherpa_lund_rjet1_Pt = ROOT.TH1D("h1_sherpa_lund_rjet1_Pt","h1_sherpa_lund_rjet1_Pt",300,0,3000)
h1_sherpa_lund_rjet2_Pt = ROOT.TH1D("h1_sherpa_lund_rjet2_Pt","h1_sherpa_lund_rjet2_Pt",300,0,3000)
#h2_track_occupancy_lund = ROOT.TH2F("h2_track_occupancy_lund","h2_track_occupancy_lund",50,-2.5,2.5,64,-3.2,3.2)
h2_track_occupancy_lund = ROOT.TH2F("h2_track_occupancy_lund","h2_track_occupancy_lund",25,-0.5,0.5,25,-0.5,0.5)

lund_jet_array = np.zeros( (t_sherpa_lund.GetEntries()*2, 100, 3 ) )
for event in range(0,t_sherpa_lund.GetEntries()):
    t_sherpa_lund.GetEntry(event)
    if(event%1000==0): print('event %s / %s SHERPA LUND',  event,t_sherpa_lund.GetEntries())
    h1_sherpa_lund_tjet1_Pt.Fill(t_sherpa_lund.tjet1_Pt,t_sherpa_lund.mcWeight)
    h1_sherpa_lund_tjet2_Pt.Fill(t_sherpa_lund.tjet2_Pt,t_sherpa_lund.mcWeight)

    # leading jet
    jet_track_array_1 = np.zeros( (100, 3) )
    if(len(t_sherpa_lund.rjet1_Pt)>0):
        h1_sherpa_lund_rjet1_Pt.Fill(t_sherpa_lund.rjet1_Pt[0],t_sherpa_lund.mcWeight)

        for track in range(0,100): # gotta zero pad
            if(track < len(t_sherpa_lund.rjet1_trk_pt[0])):
                pt = t_sherpa_lund.rjet1_trk_pt[0][track]
                eta = t_sherpa_lund.rjet1_trk_eta[0][track]
                phi = t_sherpa_lund.rjet1_trk_phi[0][track]
                E = t_sherpa_lund.rjet1_trk_E[0][track]
                jet_track_array_1[track,0] = pt
                jet_track_array_1[track,1] = eta-t_sherpa_lund.rjet1_Eta[0] #-avg_eta
                jet_track_array_1[track,2] = phi-t_sherpa_lund.rjet1_Phi[0] #-avg_phi+math.pi
                #jet_track_array_1[track,3] = E
                h2_track_occupancy_lund.Fill(eta-t_sherpa_lund.rjet1_Eta[0],
                                             phi-t_sherpa_lund.rjet1_Phi[0],
                                             pt*t_sherpa_lund.mcWeight)
            else:
                jet_track_array_1[track,0] = 0
                jet_track_array_1[track,1] = 0
                jet_track_array_1[track,2] = 0
                #jet_track_array_1[track,3] = 0
        if(len(t_sherpa_lund.rjet1_trk_pt[0])>0): jet_track_array_1[:,0]/=np.sum(jet_track_array_1[:,0],0)
        lund_jet_array[event] = jet_track_array_1

    # subleading jet
    '''
    jet_track_array_2 = np.zeros( (100, 3) )
    if(len(t_sherpa_lund.rjet2_Pt)>0):
        h1_sherpa_lund_rjet2_Pt.Fill(t_sherpa_lund.rjet2_Pt[0],t_sherpa_lund.mcWeight)

        #for track in range(0,len(t_sherpa_cluster.rjet1_trk_pt[0])):
        for track in range(0,100): # gotta zero pad
            if(track < len(t_sherpa_lund.rjet2_trk_pt[0])):
                pt = t_sherpa_lund.rjet2_trk_pt[0][track]
                eta = t_sherpa_lund.rjet2_trk_eta[0][track]
                phi = t_sherpa_lund.rjet2_trk_phi[0][track]
                E = t_sherpa_lund.rjet2_trk_E[0][track]
                jet_track_array_2[track,0] = pt*1.e3
                jet_track_array_2[track,1] = eta
                jet_track_array_2[track,2] = phi
                jet_track_array_2[track,3] = E*1.e3
                h2_track_occupancy_lund.Fill(eta,phi,pt*t_sherpa_lund.mcWeight)
            else:
                jet_track_array_2[track,0] = 0
                jet_track_array_2[track,1] = 0
                jet_track_array_2[track,2] = 0
                jet_track_array_2[track,3] = 0
    lund_jet_array[event+t_sherpa_lund.GetEntries()] = jet_track_array_2                
    '''

print(lund_jet_array)
print(lund_jet_array.shape)

h5f_lund = h5py.File('lund_jz7.h5', 'w')
h5f_lund.create_dataset('lund_sherpa_jz7', data=lund_jet_array)
h5f_lund.close()

histograms = []
histograms.append(h1_sherpa_cluster_tjet1_Pt)
histograms.append(h1_sherpa_cluster_tjet2_Pt)
histograms.append(h1_sherpa_lund_tjet1_Pt)
histograms.append(h1_sherpa_lund_tjet2_Pt)
histograms.append(h1_sherpa_cluster_rjet1_Pt)
histograms.append(h1_sherpa_cluster_rjet2_Pt)
histograms.append(h1_sherpa_lund_rjet1_Pt)
histograms.append(h1_sherpa_lund_rjet2_Pt)

file_out = ROOT.TFile("FirstLook_out.root","recreate")
for h in histograms:
    print(h.GetTitle())
    h.SetLineWidth(2)
    if("cluster" in h.GetTitle()):
        h.SetLineColor(ROOT.TColor.GetColor("#FF0000"))
    if("lund" in h.GetTitle()):
        h.SetLineColor(ROOT.TColor.GetColor("#0000FF"))
        h.SetLineStyle(2)
    h.Rebin(4)
    if(h.Integral()>0): h.Scale(1./h.Integral())
    h.Write()

h2_track_occupancy_cluster.Write()
h2_track_occupancy_lund.Write()
h2_track_occupancy_ratio = ROOT.TH2D("h2_track_occupancy_ratio","h2_track_occupancy_ratio",25,-0.5,0.5,25,-0.5,0.5)
h2_track_occupancy_ratio.Divide(h2_track_occupancy_cluster,h2_track_occupancy_lund)
h2_track_occupancy_ratio.Write()
file_out.Close()
