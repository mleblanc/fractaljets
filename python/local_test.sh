#!/bin/bash

#xAH_run.py --config src/fractaljets/python/xAH_Trees_10LC.py \
#    --files mc16_13TeV:mc16_13TeV.364707.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7WithSW.deriv.DAOD_JETM6.e7142_s3126_r10724_p3749/ \
#    --inputRucio \
#    --nevents 5000 \
#    -f \
#    direct

xAH_run.py --config src/fractaljets/python/xAH_Trees_10LC_Data.py \
    --files data18_13TeV:data18_13TeV.periodC.physics_Main.PhysCont.DAOD_JETM6.grp18_v01_p3713/ \
    --inputRucio \
    --nevents 2000 \
    -f \
    direct
