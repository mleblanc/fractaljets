#!/bin/bash

xAH_run.py --config src/fractaljets/python/xAH_Trees.py \
    --files user.mleblanc.ModelA.JETM6.v2_EXT0/ \
    --inputRucio \
    -f \
    --submitDir local_ModelA/ \
    direct
