#!/bin/bash

xAH_run.py --config src/fractaljets/python/xAH_Trees.py \
    --files user.mleblanc.ModelA.JETM6_EXT0/ \
    -f \
    prun \
    --optGridOutputSampleName "user.mleblanc.%in:name[2]%.%in:name[3]%.MODELAv00"    
