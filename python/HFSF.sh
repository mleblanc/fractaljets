#!/bin/bash

#xAH_run.py --config src/fractaljets/python/xAH_Trees_10LC.py \
#    --files src/fractaljets/data/HFSF.txt \
#    --inputList \
#    --inputRucio \
#    -f \
#    prun \
#    --optGridOutputSampleName "user.mleblanc.%in:name[2]%.%in:name[3]%.FRACTALSv22" \
#    --optGridNFilesPerJob 1 \
#    --optGridCloud "US"

xAH_run.py --config src/fractaljets/python/xAH_Trees_10LC_Data.py \
    --files src/fractaljets/data/HFSF_Data18.txt \
    --inputList \
    --inputRucio \
    -f \
    prun \
    --optGridOutputSampleName "user.mleblanc.%in:name[2]%.%in:name[3]%.FRACTALSv22" \
    --optGridCloud "US" 
    #--optGridNFilesPerJob 1 \
