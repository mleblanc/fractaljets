#!/bin/bash

"""
xAH_run.py --config src/fractaljets/python/xAH_Trees_10LC_Data.py \
    --files src/fractaljets/data/DAOD_JETM6_2018_p3713.txt \
    --inputList \
    --inputRucio \
    -f \
    --nevents 1000 \
    prun \
    --optGridOutputSampleName "user.mleblanc.%in:name[2]%.%in:name[3]%.DATA18v01"

xAH_run.py --config src/fractaljets/python/xAH_Trees_10LC.py \
    --files src/fractaljets/data/DAOD_JETM6_Pythia_p3916.txt \
    --inputList \
    --inputRucio \
    -f \
    prun \
    --optGridOutputSampleName "user.mleblanc.%in:name[2]%.%in:name[3]%.PYTHIAv01"

xAH_run.py --config src/fractaljets/python/xAH_Trees_10LC.py \
    --files src/fractaljets/data/boosted_ws.txt \
    --inputList \
    --inputRucio \
    -f \
    prun \
    --optGridOutputSampleName "user.mleblanc.%in:name[2]%.%in:name[3]%.WPRIMEv01"    

xAH_run.py --config src/fractaljets/python/xAH_Trees_10LC.py \
    --files src/fractaljets/data/boosted_tops.txt \
    --inputList \
    --inputRucio \
    -f \
    prun \
    --optGridOutputSampleName "user.mleblanc.%in:name[2]%.%in:name[3]%.ZPRIMEv01"
"""

