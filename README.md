# FractalJets

## Setup

To get up and running, do the following ...

```
mkdir build src
cd src/
# Need to use 21.2.90 for SL6 support for now
asetup AnalysisBase,21.2.90,here
git clone git@github.com:UCATLAS/xAODAnaHelpers.git
# Need to use last xAH commit with SL6 support for now
cd xAODAnaHelpers/; git checkout aaf7fc3fde9819bcb5cc3737df0226e275110671; cd ../
git clone ssh://git@gitlab.cern.ch:7999/mleblanc/fractaljets.git
git clone git@github.com:mattleblanc/IDTrackSel.git
cd ../build/
cmake ../src; make
cd ../
source build/x8*/setup.sh
```

To run a test and see if everything is working, try to do ...

```
source src/fractaljets/python/local_test.sh
```

