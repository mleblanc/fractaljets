import os

inputList = ['AntiKt10CSSKUFO']
groomList = ['', 'TrimmedPtFrac5SmallR20', 'PrunedZCut15RCut25', 'SoftDropZCut10Beta0', 'SoftDropZCut10Beta100', 'BottomUpSoftDropZCut5Beta100', 'RecursiveSoftDropZCut5Beta100N-100', 'RecursiveSoftDropZCut5Beta100N100', 'RecursiveSoftDropZCut5Beta100N300']

jetList = []
for input in inputList:
    for groom in groomList:
        jetList.append(input+groom)
print(jetList)

path = '/data0/users/sweber/'

for input in jetList:
    truthInput = input.replace('AntiKt10CSSKUFO', 'AntiKt10Truth')
    command = 'python scripts/runEMDchunk.py --infile1 '+path+input+' --infile2 '+path+truthInput
    os.system(command)
