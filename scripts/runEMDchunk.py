import argparse
import os

parser = argparse.ArgumentParser("runEMDchunk")
parser.add_argument('--infile1', type=str)
parser.add_argument('--infile2', type=str)
myargs = parser.parse_args()

print('python fractaljets/ml/EMDchunk.py --infile1 '+myargs.infile1+'.h5 --infile2 '+myargs.infile2+'.h5 --label1 '+myargs.infile1+' --label2 '+myargs.infile2+' --chunkSize 10000')
os.system('python fractaljets/ml/EMDchunk.py --infile1 '+myargs.infile1+'.h5 --infile2 '+myargs.infile2+'.h5 --label1 '+myargs.infile1+' --label2 '+myargs.infile2+' --chunkSize 10000')
