import os

#inputList = ['AntiKt10Truth', 'AntiKt10OrigLCTopo', 'AntiKt10CSSKUFO']
inputList = ['AntiKt10Truth', 'AntiKt10CSSKUFO']
groomList = ['', 'TrimmedPtFrac5SmallR20', 'PrunedZCut15RCut25', 'SoftDropZCut10Beta0', 'SoftDropZCut10Beta100', 'BottomUpSoftDropZCut5Beta100', 'RecursiveSoftDropZCut5Beta100N-100', 'RecursiveSoftDropZCut5Beta100N100', 'RecursiveSoftDropZCut5Beta100N300']
#groomList = ['', 'PrunedZCut15RCut25', 'TrimmedPtFrac5SmallR20']

jetList = []
for input in inputList:
    for groom in groomList:
        jetList.append(input+groom)
print(jetList)

n = 1

for i,input1 in enumerate(jetList):
    for j,input2 in enumerate(jetList):
        if(j > i):
            break
        outfileName = input1+'_0_'+input2+'_0'
        command = 'prun --containerImage docker://sweber613/fractaljets:HFSF2019 --exec "git clone https://gitlab.cern.ch/mleblanc/fractaljets.git; ls; python fractaljets/scripts/runEMDchunk.py --infile1 '+input1+' --infile2 '+input2+'; ls" --outputs '+outfileName+'.h5 --outDS user.stephen.Jan7_'+str(n)+'/ --inDS user.stephen:user.stephen.HFSFh5 --forceStaged --excludedSite ANALY_RAL'
        n += 1;

        print(command)
        os.system(command)
